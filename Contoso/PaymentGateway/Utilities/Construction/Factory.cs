using System;
using Contoso.PaymentGateway.Utilities.Interfaces.Construction;

namespace Contoso.PaymentGateway.Utilities.Construction
{
    public class Factory<T> : IFactory<T>
    {
        private readonly Func<T> createFunction;

        public Factory(Func<T> createFunction)
        {
            this.createFunction = createFunction;
        }

        public T Create()
        {
            return createFunction.Invoke();
        }
    }

    public class Factory<T1, T2> : IFactory<T1, T2>
    {
        private readonly Func<T1, T2> createFunction;

        public Factory(Func<T1, T2> createFunction)
        {
            this.createFunction = createFunction;
        }

        public T2 Create(T1 p1)
        {
            return createFunction.Invoke(p1);
        }
    }

    public class Factory<T1, T2, T3> : IFactory<T1, T2, T3>
    {
        private readonly Func<T1, T2, T3> createFunction;

        public Factory(Func<T1, T2, T3> createFunction)
        {
            this.createFunction = createFunction;
        }

        public T3 Create(T1 p1, T2 p2)
        {
            return createFunction.Invoke(p1, p2);
        }
    }
}