using System;
using Contoso.PaymentGateway.Utilities.Interfaces.Identity;

namespace Contoso.PaymentGateway.Utilities.Identity
{
    public class GuidIdentityGenerator : IIdentityGenerator<Guid>
    {
        public Guid GenerateId()
        {
            return Guid.NewGuid();
        }
    }
}