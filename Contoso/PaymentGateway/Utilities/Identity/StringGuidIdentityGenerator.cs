using Contoso.PaymentGateway.Utilities.Interfaces.Identity;

namespace Contoso.PaymentGateway.Utilities.Identity
{
    /// <summary>
    ///     This class generates a new Guid and returns it as a string.
    /// </summary>
    public class StringGuidIdentityGenerator : IIdentityGenerator<string>
    {
        private readonly GuidIdentityGenerator guidIdentityGenerator;

        public StringGuidIdentityGenerator(GuidIdentityGenerator guidIdentityGenerator)
        {
            this.guidIdentityGenerator = guidIdentityGenerator;
        }

        /// <summary>
        ///     Generates a new Guid and returns it as a string.
        /// </summary>
        /// <returns>The string representation of a new Guid</returns>
        public string GenerateId()
        {
            return guidIdentityGenerator.GenerateId().ToString();
        }
    }
}