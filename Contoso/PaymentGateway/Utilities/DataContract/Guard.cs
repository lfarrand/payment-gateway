using System;
using Contoso.PaymentGateway.Utilities.Exceptions;

namespace Contoso.PaymentGateway.Utilities.DataContract
{
    /// <summary>
    ///     This class is to be used to validate that input into a function
    ///     meets expectations.
    /// </summary>
    public class Guard
    {
        public void ThrowIfNull(params Tuple<string, object>[] args)
        {
            foreach (var (parameterName, obj) in args)
            {
                if (obj == null)
                {
                    throw new PaymentGatewayArgumentNullException($"Parameter {parameterName} was null");
                }
            }
        }
    }
}