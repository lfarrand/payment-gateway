using System;
using System.Runtime.Serialization;

namespace Contoso.PaymentGateway.Utilities.Exceptions
{
    public class PaymentGatewayArgumentNullException : PaymentGatewayException
    {
        public PaymentGatewayArgumentNullException()
        {
        }

        protected PaymentGatewayArgumentNullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public PaymentGatewayArgumentNullException(string message) : base(message)
        {
        }

        public PaymentGatewayArgumentNullException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}