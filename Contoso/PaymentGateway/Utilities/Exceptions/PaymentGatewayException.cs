using System;
using System.Runtime.Serialization;

namespace Contoso.PaymentGateway.Utilities.Exceptions
{
    public class PaymentGatewayException : ApplicationException
    {
        public PaymentGatewayException()
        {
        }

        protected PaymentGatewayException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public PaymentGatewayException(string message) : base(message)
        {
        }

        public PaymentGatewayException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}