using System;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;

namespace Contoso.PaymentGateway.Utilities.Logging
{
    /// <summary>
    ///     This no-op class is designed to be used in tests, to speed up
    ///     test execution.
    /// </summary>
    public class NullLogger : ILog
    {
        public void Debug(string message)
        {
            // Do nothing
        }

        public void Info(string message)
        {
            // Do nothing
        }

        public void Warn(string message)
        {
            // Do nothing
        }

        public void Error(string message)
        {
            // Do nothing
        }

        public void Error(Exception exception, string message)
        {
            // Do nothing
        }
    }
}