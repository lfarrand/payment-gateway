using System;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;

namespace Contoso.PaymentGateway.Utilities.Logging
{
    /// <summary>
    ///     This class is designed to be used when debugging tests.
    ///     It's a lightweight wrapper around Console.WriteLine.
    /// </summary>
    public class ConsoleLogger : ILog
    {
        public void Debug(string message)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(message);
        }

        public void Info(string message)
        {
            Console.WriteLine(message);
        }

        public void Warn(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
        }

        public void Error(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
        }

        public void Error(Exception exception, string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.WriteLine(exception);
        }
    }
}