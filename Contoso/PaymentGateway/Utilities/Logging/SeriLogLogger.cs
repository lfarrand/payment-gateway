using System;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace Contoso.PaymentGateway.Utilities.Logging
{
    /// <summary>
    ///     This logger is designed to be used for live applications.
    /// </summary>
    public class SeriLogLogger : ILog
    {
        private readonly ILogger logger;

        public SeriLogLogger(IConfiguration configuration)
        {
            logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(configuration)
                    .CreateLogger();
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Info(string message)
        {
            logger.Information(message);
        }

        public void Warn(string message)
        {
            logger.Warning(message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Error(Exception exception, string message)
        {
            logger.Error(exception, message);
        }
    }
}