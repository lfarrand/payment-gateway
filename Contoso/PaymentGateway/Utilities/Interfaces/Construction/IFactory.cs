namespace Contoso.PaymentGateway.Utilities.Interfaces.Construction
{
    public interface IFactory<out T>
    {
        T Create();
    }

    public interface IFactory<in T1, out T2>
    {
        T2 Create(T1 p1);
    }

    public interface IFactory<in T1, in T2, out T3>
    {
        T3 Create(T1 p1, T2 p2);
    }
}