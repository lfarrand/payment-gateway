using System;

namespace Contoso.PaymentGateway.Utilities.Interfaces.Logging
{
    public interface ILog
    {
        void Debug(string message);

        void Info(string message);

        void Warn(string message);

        void Error(string message);

        void Error(Exception exception, string message);
    }
}