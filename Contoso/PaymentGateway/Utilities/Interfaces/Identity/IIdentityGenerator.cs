namespace Contoso.PaymentGateway.Utilities.Interfaces.Identity
{
    public interface IIdentityGenerator<out TKey>
    {
        TKey GenerateId();
    }
}