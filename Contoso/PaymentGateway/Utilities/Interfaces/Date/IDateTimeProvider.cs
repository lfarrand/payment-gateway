using System;

namespace Contoso.PaymentGateway.Utilities.Interfaces.Date
{
    /// <summary>
    ///     The purpose of this interface is to eliminate the dependency
    ///     on time within tests by abstracting the notion of the current
    ///     time.
    /// </summary>
    public interface IDateTimeProvider
    {
        DateTime Current { get; }
    }
}