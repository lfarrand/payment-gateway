using System;
using Contoso.PaymentGateway.Utilities.Interfaces.Date;

namespace Contoso.PaymentGateway.Utilities.Date
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime Current => DateTime.Now;
    }
}