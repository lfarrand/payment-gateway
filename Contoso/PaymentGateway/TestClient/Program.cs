﻿using System;
using System.Net;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Utilities.Identity;
using RestSharp;

namespace Contoso.PaymentGateway.TestClient
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine($"Running on host {Environment.MachineName}");

            var url = "https://localhost:5001/api";

            if (args[0] == "CreatePaymentCard")
            {
                var idGenerator = new StringGuidIdentityGenerator(new GuidIdentityGenerator());
                var client = new RestClient(url);
                // client.Authenticator = new HttpBasicAuthenticator(username, password);

                var createPaymentCardRequest = new RestRequest("paymentcard", Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                var paymentCard = new PaymentCard(idGenerator.GenerateId(), "Card Alias", "Visa",
                                                  "4111111111111111", "01",
                                                  "2020", "Card Holder Name");

                createPaymentCardRequest.AddJsonBody(paymentCard);

                var createPaymentCardResponse = client.Execute<CreatePaymentCardResponse>(createPaymentCardRequest);
                var paymentCardId = createPaymentCardResponse.Data.PaymentCardId;

                if (createPaymentCardResponse.ResponseStatus == ResponseStatus.Completed &&
                    createPaymentCardResponse.StatusCode     == HttpStatusCode.OK)
                {
                    Console.WriteLine($"Created payment card with id {paymentCardId}");
                }

                var getAllPaymentCardsRequest = new RestRequest("paymentcard", Method.GET);
                var getAllPaymentCardsResponse = client.Execute<GetAllPaymentCardsResponse>(getAllPaymentCardsRequest);

                if (getAllPaymentCardsResponse.ResponseStatus == ResponseStatus.Completed &&
                    getAllPaymentCardsResponse.StatusCode     == HttpStatusCode.OK)
                {
                    Console.WriteLine("The following payment cards exist:");
                    foreach (var card in getAllPaymentCardsResponse.Data.PaymentCards)
                    {
                        Console.WriteLine($"\tPayment card with id {card.Id}");
                    }
                }

                var getPaymentCardRequest = new RestRequest($"paymentcard/{paymentCardId}", Method.GET);
                var getPaymentCardResponse = client.Execute<GetPaymentCardResponse>(getPaymentCardRequest);

                if (getPaymentCardResponse.ResponseStatus == ResponseStatus.Completed &&
                    getPaymentCardResponse.StatusCode     == HttpStatusCode.OK)
                {
                    if (getPaymentCardResponse.Data.PaymentCard.Id != paymentCard.Id)
                    {
                        Console.WriteLine("Payment card is not the same");
                    }
                    else
                    {
                        Console.WriteLine($"Payment card with id {paymentCard.Id} is the same");
                    }
                }
            }
        }
    }
}