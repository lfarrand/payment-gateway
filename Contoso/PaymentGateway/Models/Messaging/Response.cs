namespace Contoso.PaymentGateway.Models.Messaging
{
    public class Response
    {
        public string UniqueId { get; set; }

        public bool Success { get; set; }
    }
}