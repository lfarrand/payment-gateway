using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.Models.Domain.Payments
{
    public class Payment : IEntity<string>
    {
        public Payment(string id, decimal amount, string currency, string description)
        {
            Id = id;
            Amount = amount;
            Currency = currency;
            Description = description;
        }

        public Payment()
        {
        }

        public decimal Amount { get; set; }

        public string Currency { get; set; }

        public string Description { get; set; }

        public string Id { get; set; }
    }
}