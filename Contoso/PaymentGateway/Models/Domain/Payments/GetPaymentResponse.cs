using Contoso.PaymentGateway.Models.Messaging;

namespace Contoso.PaymentGateway.Models.Domain.Payments
{
    public class GetPaymentResponse : Response
    {
        public Payment Payment { get; set; }
    }
}