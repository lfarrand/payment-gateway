namespace Contoso.PaymentGateway.Models.Domain.Payments
{
    public enum ProcessPaymentResultStatusCode
    {
        Success = 100,
        Failed = 200
    }
}