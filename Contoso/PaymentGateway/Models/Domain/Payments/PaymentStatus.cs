namespace Contoso.PaymentGateway.Models.Domain.Payments
{
    public enum PaymentStatus
    {
        Successful = 100,
        NotSuccessful = 200,
        NotSuccessfulCardInvalid = 210,
        NOtSuccessfulCardExpired = 220
    }
}