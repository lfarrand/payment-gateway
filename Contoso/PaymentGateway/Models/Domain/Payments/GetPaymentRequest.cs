namespace Contoso.PaymentGateway.Models.Domain.Payments
{
    public class GetPaymentRequest
    {
        public string PaymentId { get; set; }
    }
}