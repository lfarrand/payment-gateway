namespace Contoso.PaymentGateway.Models.Domain.Payments
{
    public enum MakePaymentResponseCode
    {
        Success = 100,
        FailedPaymentCardNotFound = 200,
        FailedPaymentCardIssuerError = 220,
        FailedUnknownError = 500
    }
}