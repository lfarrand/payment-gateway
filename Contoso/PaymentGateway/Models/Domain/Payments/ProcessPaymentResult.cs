namespace Contoso.PaymentGateway.Models.Domain.Payments
{
    public class ProcessPaymentResult
    {
        public string PaymentId { get; set; }

        public string UniqueId { get; set; }

        public bool Success { get; set; }

        public ProcessPaymentResultStatusCode StatusCode { get; set; }

        public string Message { get; set; }
    }
}