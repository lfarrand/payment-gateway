using Contoso.PaymentGateway.Models.Messaging;

namespace Contoso.PaymentGateway.Models.Domain.CardPayments
{
    public class GetCardPaymentResponse : Response
    {
        public CardPayment CardPayment { get; set; }
    }
}