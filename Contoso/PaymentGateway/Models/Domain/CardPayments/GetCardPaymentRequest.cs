namespace Contoso.PaymentGateway.Models.Domain.CardPayments
{
    public class GetCardPaymentRequest
    {
        public string CardPaymentId { get; set; }
    }
}