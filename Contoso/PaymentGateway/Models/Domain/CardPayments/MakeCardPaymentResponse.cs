using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Models.Messaging;

namespace Contoso.PaymentGateway.Models.Domain.CardPayments
{
    public class MakeCardPaymentResponse : Response
    {
        public string PaymentId { get; set; }

        public MakePaymentResponseCode ResponseCode { get; set; }

        public string Message { get; set; }
    }
}