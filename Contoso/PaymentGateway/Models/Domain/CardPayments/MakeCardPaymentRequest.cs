using Contoso.PaymentGateway.Models.Domain.Payments;

namespace Contoso.PaymentGateway.Models.Domain.CardPayments
{
    public class MakeCardPaymentRequest
    {
        public Payment Payment { get; set; }

        public string PaymentCardId { get; set; }
    }
}