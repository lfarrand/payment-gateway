using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.Models.Domain.CardPayments
{
    public class CardPayment : IEntity<string>
    {
        public CardPayment()
        {
        }

        public CardPayment(string id, PaymentCard paymentCard, Payment payment, string cardVerificationValue)
        {
            Id = id;
            PaymentCard = paymentCard;
            Payment = payment;
            CardVerificationValue = cardVerificationValue;
        }

        public PaymentCard PaymentCard { get; set; }

        public Payment Payment { get; set; }

        public string CardVerificationValue { get; set; }

        public string Id { get; set; }
    }
}