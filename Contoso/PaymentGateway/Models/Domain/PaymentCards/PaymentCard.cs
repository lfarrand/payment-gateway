using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.Models.Domain.PaymentCards
{
    public class PaymentCard : IEntity<string>
    {
        public PaymentCard(string id, string alias, string type, string cardNumber, string expiryMonth,
                           string expiryYear,
                           string cardHolderName)
        {
            Id = id;
            Alias = alias;
            Type = type;
            CardNumber = cardNumber;
            ExpiryMonth = expiryMonth;
            ExpiryYear = expiryYear;
            CardHolderName = cardHolderName;
        }

        public PaymentCard()
        {
        }

        public string Alias { get; set; }

        /// <summary>
        ///     The type of card: Visa, MasterCard etc.
        /// </summary>
        public string Type { get; set; }

        public string CardNumber { get; set; }

        public string ExpiryMonth { get; set; }

        public string ExpiryYear { get; set; }

        public string CardHolderName { get; set; }

        public string Id { get; set; }
    }
}