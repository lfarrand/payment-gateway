namespace Contoso.PaymentGateway.Models.Domain.PaymentCards
{
    public class CreatePaymentCardRequest
    {
        public PaymentCard PaymentCard { get; set; }
    }
}