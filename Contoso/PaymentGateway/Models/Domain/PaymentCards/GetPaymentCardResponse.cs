using Contoso.PaymentGateway.Models.Messaging;

namespace Contoso.PaymentGateway.Models.Domain.PaymentCards
{
    public class GetPaymentCardResponse : Response
    {
        public PaymentCard PaymentCard { get; set; }
    }
}