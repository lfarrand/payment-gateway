namespace Contoso.PaymentGateway.Models.Domain.PaymentCards
{
    public class GetPaymentCardRequest
    {
        public string PaymentCardId { get; set; }
    }
}