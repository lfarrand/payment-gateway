using Contoso.PaymentGateway.Models.Messaging;

namespace Contoso.PaymentGateway.Models.Domain.PaymentCards
{
    public class CreatePaymentCardResponse : Response
    {
        public string PaymentCardId { get; set; }
    }
}