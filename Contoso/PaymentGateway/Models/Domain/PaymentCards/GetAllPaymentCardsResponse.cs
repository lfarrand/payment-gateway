using System.Collections.Generic;
using Contoso.PaymentGateway.Models.Messaging;

namespace Contoso.PaymentGateway.Models.Domain.PaymentCards
{
    public class GetAllPaymentCardsResponse : Response
    {
        public IList<PaymentCard> PaymentCards { get; set; }
    }
}