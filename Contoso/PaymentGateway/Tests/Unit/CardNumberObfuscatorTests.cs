using Contoso.PaymentGateway.BusinessLogic.CardPayments;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Unit
{
    /// <summary>
    ///     I created this class to demonstrate usage of xUnit.
    ///     I used NUnit for the other sets of tests because that
    ///     was what I am most familiar with, but after our last
    ///     talk I decided to invest some time to read about xUnit.
    ///     I decided that it was a better tool because it was built
    ///     with .Net in mind, and also it strictly forbids the usage
    ///     of SetUp and TearDown methods, which encourage sharing
    ///     state between tests.
    ///     In my option sharing state between tests is a bad thing
    ///     because it makes the tests hard to follow and you will also
    ///     encounter flaky tests when you start running them in parallel.
    ///     Enforcing this behaviour can only be a good thing.
    ///     Edit: I've now migrated all of the other tests from NUnit to xUnit.
    /// </summary>
    public class CardNumberObfuscatorTests
    {
        [Theory]
        [ClassData(typeof(CardNumberTestCases))]
        public void WhenObfuscatingCardNumber_NumberShouldBeMasked(string inputCardNumber, string expectedCardNumber)
        {
            var cardNumberObfuscator = new CardNumberObfuscator();

            var actualCardNumber = cardNumberObfuscator.ObfuscateCardNumber(inputCardNumber);

            actualCardNumber.Should().Be(expectedCardNumber);
        }

        private class CardNumberTestCases : TheoryData<string, string>
        {
            public CardNumberTestCases()
            {
                Add("4111111111111111", "XXXXXXXXXXXX1111");
                Add("41111",            "X1111");
                Add("4111",             "4111");
                Add("411",              "XXXXXXXXXXXXXXXX");
                Add("",                 "XXXXXXXXXXXXXXXX");
                Add(null,               "XXXXXXXXXXXXXXXX");
            }
        }
    }
}