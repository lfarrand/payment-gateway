using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class ExpressionConverterTests
    {
        private class Model
        {
            public string Value { get; set; }
        }

        private class Data
        {
            public string Value { get; set; }
        }

        private IMapper CreateMapper()
        {
            var config = new MapperConfiguration(mc =>
            {
                mc.CreateMap<Model, Data>();
                mc.CreateMap<Data, Model>();
                mc.AddExpressionMapping();
            });

            config.AssertConfigurationIsValid();

            config.CompileMappings();

            return config.CreateMapper();
        }

        [Fact]
        public void WhenFilteringList_ShouldNotBeCaseSensitive()
        {
            // Arrange
            var dataList = new List<Data> {new Data {Value = "A"}, new Data {Value = "B"}, new Data {Value = "C"}};

            Expression<Func<Model, bool>> predicate = model =>
                model.Value.Equals("A", StringComparison.CurrentCultureIgnoreCase);

            var dataPredicate = CreateMapper().Map<Expression<Func<Data, bool>>>(predicate)
                                              .Compile();

            // Act
            var filtered = dataList.Where(dataPredicate).ToList();

            // Assert
            filtered.Count.Should().Be(1);
            filtered[0].Value.Should().Be("A");
        }

        [Fact]
        public void WhenFilteringListUsingLowerCase_ShouldBeCaseSensitive()
        {
            // Arrange
            var dataList = new List<Data> {new Data {Value = "A"}, new Data {Value = "B"}, new Data {Value = "C"}};

            Expression<Func<Model, bool>> predicate = model =>
                string.Equals(model.Value, "a", StringComparison.InvariantCulture);

            var dataPredicate = CreateMapper().Map<Expression<Func<Data, bool>>>(predicate)
                                              .Compile();

            // Act
            var filtered = dataList.Where(dataPredicate).ToList();

            // Assert
            filtered.Count.Should().Be(0);
        }

        [Fact]
        public void WhenFilteringListUsingStringEquals_ShouldBeCaseSensitive()
        {
            // Arrange
            var dataList = new List<Data> {new Data {Value = "A"}, new Data {Value = "B"}, new Data {Value = "C"}};

            Expression<Func<Model, bool>> predicate = model => model.Value == "A";

            var dataPredicate = CreateMapper().Map<Expression<Func<Data, bool>>>(predicate)
                                              .Compile();

            // Act
            var filtered = dataList.Where(dataPredicate).ToList();

            // Assert
            filtered.Count.Should().Be(1);
            filtered[0].Value.Should().Be("A");
        }

        [Fact]
        public void WhenFilteringListUsingUpperCase_ShouldBeCaseSensitive()
        {
            // Arrange
            var dataList = new List<Data> {new Data {Value = "A"}, new Data {Value = "B"}, new Data {Value = "C"}};

            Expression<Func<Model, bool>> predicate = model =>
                string.Equals(model.Value, "A", StringComparison.InvariantCulture);

            var dataPredicate = CreateMapper().Map<Expression<Func<Data, bool>>>(predicate)
                                              .Compile();

            // Act
            var filtered = dataList.Where(dataPredicate).ToList();

            // Assert
            filtered.Count.Should().Be(1);
            filtered[0].Value.Should().Be("A");
        }

        [Fact]
        public void WhenMappingFromDataToModel_ShouldMapExpression()
        {
            Expression<Func<Data, bool>> dtoExpression = dto => dto.Value == "A";
            var modelExpression = CreateMapper().Map<Expression<Func<Model, bool>>>(dtoExpression);

            modelExpression.Should().NotBeNull();
        }
    }
}