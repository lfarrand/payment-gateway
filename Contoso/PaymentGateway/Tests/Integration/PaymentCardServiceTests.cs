using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Orchestrator;
using Contoso.PaymentGateway.DataAccess.DataStore;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class PaymentCardOrchestratorTests
    {
        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        public async Task WhenCreatingPaymentCard_ShouldSucceed(RepositoryTestContext ctx)
        {
            var paymentCardDataStore = new PaymentCardDataStore(ctx.RepositoryFactory);
            var paymentCardOrchestrator = new PaymentCardOrchestrator(ctx.Log, ctx.DateTimeProvider, paymentCardDataStore);
            var paymentCard = new PaymentCard(ctx.IdGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111",
                                              "01", "2020", "Card Holder Name");
            var request = new CreatePaymentCardRequest {PaymentCard = paymentCard};
            var response = await paymentCardOrchestrator.CreatePaymentCard(request);

            response.Success.Should().BeTrue();
            response.PaymentCardId.Should().NotBeEmpty();
        }

        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        public async Task WhenGettingPaymentCard_ShouldSucceed(RepositoryTestContext ctx)
        {
            GetPaymentCardResponse response;
            PaymentCard paymentCard;

            using (var repository = ctx.RepositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                var paymentCardDataStore = new PaymentCardDataStore(ctx.RepositoryFactory);
                var paymentCardOrchestrator = new PaymentCardOrchestrator(ctx.Log, ctx.DateTimeProvider, paymentCardDataStore);
                paymentCard = new PaymentCard(ctx.IdGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111", "01",
                                              "2020", "Card Holder Name");
                await repository.CreateAsync(paymentCard);
                await repository.SaveChangesAsync();

                var request = new GetPaymentCardRequest {PaymentCardId = paymentCard.Id};
                response = await paymentCardOrchestrator.GetPaymentCard(request);
            }

            response.Success.Should().BeTrue();
            response.PaymentCard.Alias.Should().Be(paymentCard.Alias);
            response.PaymentCard.Id.Should().Be(paymentCard.Id);
        }
    }
}