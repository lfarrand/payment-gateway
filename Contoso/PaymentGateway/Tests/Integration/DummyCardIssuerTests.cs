using Contoso.PaymentGateway.BusinessLogic.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class DummyCardIssuerTests
    {
        [Fact]
        public void WhenValidatePayment_ShouldSucceed()
        {
            var ctx = new TestContext();
            var statusCodeMapping = new ChargeCardOperationStatusMappingFactory().Create();
            var cardIssuerApi = new DummyCardIssuerApi(ctx.IdGenerator);
            ICardPaymentProcessor cardPaymentProcessor =
                new CardPaymentProcessor(ctx.Log, ctx.DateTimeProvider, cardIssuerApi, statusCodeMapping);

            var payment = new Payment(ctx.IdGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(ctx.IdGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111", "01",
                                              "2020", "Card Holder Name");

            var result = cardPaymentProcessor.ProcessPayment(paymentCard, payment);

            result.Success.Should().BeTrue();
            result.StatusCode.Should().Be(ProcessPaymentResultStatusCode.Success);
        }
    }
}