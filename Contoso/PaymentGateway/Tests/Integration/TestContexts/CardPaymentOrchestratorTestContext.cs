using Contoso.PaymentGateway.BusinessLogic.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Orchestrator;
using Contoso.PaymentGateway.DataAccess.Concurrency;
using Contoso.PaymentGateway.DataAccess.DataStore;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Moq;

namespace Contoso.PaymentGateway.Tests.Integration.TestContexts
{
    public class CardPaymentOrchestratorTestContext : TestContext
    {
        public CardPaymentOrchestratorTestContext(IRepositoryFactory repositoryFactory)
        {
            var statusCodeMapping = new ChargeCardOperationStatusMappingFactory().Create();
            var cardIssuerApi = Mock.Of<ICardIssuerApi>(x =>
                                                            x.ChargeCard(It.IsAny<PaymentCard>(), It.IsAny<Payment>()) ==
                                                            new ChargeCardResult(IdGenerator.GenerateId(), ChargeCardOperationStatus.Success));

            var cardPaymentProcessor =
                new CardPaymentProcessor(Log, DateTimeProvider, cardIssuerApi, statusCodeMapping);
            var cardNumberObfuscator = new CardNumberObfuscator();
            var cardPaymentDatastore = new CardPaymentDataStore(repositoryFactory);
            var paymentCardDatastore = new PaymentCardDataStore(repositoryFactory);
            var paymentDatastore = new PaymentDataStore(repositoryFactory);
            var lockManager = new LockManager();
            CardPaymentOrchestrator =
                new CardPaymentOrchestrator(Log, cardPaymentProcessor, cardPaymentDatastore, paymentCardDatastore,
                                            paymentDatastore, cardNumberObfuscator, lockManager);

            RepositoryFactory = repositoryFactory;
        }

        public IRepositoryFactory RepositoryFactory { get; }

        public CardPaymentOrchestrator CardPaymentOrchestrator { get; }
    }
}