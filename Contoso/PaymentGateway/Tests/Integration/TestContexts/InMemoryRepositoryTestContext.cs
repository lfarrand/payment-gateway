using Contoso.PaymentGateway.DataAccess.Repository;

namespace Contoso.PaymentGateway.Tests.Integration.TestContexts
{
    public class InMemoryRepositoryTestContext : RepositoryTestContext
    {
        public InMemoryRepositoryTestContext()
        {
            RepositoryFactory = CreateRepositoryFactory();
        }

        private InMemoryRepositoryFactory CreateRepositoryFactory()
        {
            return new InMemoryRepositoryFactory(Mapper);
        }
    }
}