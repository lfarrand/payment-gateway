using Contoso.PaymentGateway.DataAccess.EntityFramework.DatabaseContext;
using Contoso.PaymentGateway.DataAccess.EntityFramework.Interfaces.DatabaseContext;
using Contoso.PaymentGateway.DataAccess.EntityFramework.Repository;

namespace Contoso.PaymentGateway.Tests.Integration.TestContexts
{
    public class EntityFrameworkRepositoryTestContext : RepositoryTestContext
    {
        public EntityFrameworkRepositoryTestContext()
        {
            DbContextFactory = new SqlLitePaymentGatewayDbContextFactory();
            RepositoryFactory = new EntityFrameworkRepositoryFactory(Mapper, DbContextFactory, new EntityFrameworkRepositoriesRegistry().Create());
        }

        public IDbContextFactory DbContextFactory { get; }
    }
}