using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Contoso.PaymentGateway.BusinessLogic.Mapping;
using Contoso.PaymentGateway.Utilities.Date;
using Contoso.PaymentGateway.Utilities.Identity;
using Contoso.PaymentGateway.Utilities.Interfaces.Date;
using Contoso.PaymentGateway.Utilities.Interfaces.Identity;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;
using Contoso.PaymentGateway.Utilities.Logging;

namespace Contoso.PaymentGateway.Tests.Integration.TestContexts
{
    public class TestContext
    {
        public TestContext()
        {
            Mapper = CreateMapper();

            Log = new ConsoleLogger();
            IdGenerator = new StringGuidIdentityGenerator(new GuidIdentityGenerator());
            DateTimeProvider = new DateTimeProvider();
        }

        public ILog Log { get; }

        public IIdentityGenerator<string> IdGenerator { get; }

        public IDateTimeProvider DateTimeProvider { get; }

        public IMapper Mapper { get; }

        private IMapper CreateMapper()
        {
            var config = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperModelMappings());
                mc.AddExpressionMapping();
            });

            config.AssertConfigurationIsValid();

            config.CompileMappings();

            return config.CreateMapper();
        }
    }
}