using System;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;

namespace Contoso.PaymentGateway.Tests.Integration.TestContexts
{
    public abstract class RepositoryTestContext : TestContext, IDisposable
    {
        public IRepositoryFactory RepositoryFactory { get; set; }

        public void Dispose()
        {
            RepositoryFactory?.Dispose();
        }
    }
}