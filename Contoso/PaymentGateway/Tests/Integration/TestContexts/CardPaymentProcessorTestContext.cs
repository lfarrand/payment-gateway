using System.Collections.Generic;
using Contoso.PaymentGateway.BusinessLogic.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.Models.Domain.Payments;

namespace Contoso.PaymentGateway.Tests.Integration.TestContexts
{
    public class CardPaymentProcessorTestContext : TestContext
    {
        public CardPaymentProcessorTestContext()
        {
            StatusCodeMapping = new ChargeCardOperationStatusMappingFactory().Create();
        }

        public Dictionary<ChargeCardOperationStatus, ProcessPaymentResultStatusCode> StatusCodeMapping { get; }
    }
}