using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using Contoso.PaymentGateway.Utilities.Identity;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class ModelMappingTests
    {
        [Fact]
        public void WhenMappingCardPayment_ShouldMapBothWays()
        {
            var ctx = new TestContext();
            var idGenerator = new StringGuidIdentityGenerator(new GuidIdentityGenerator());
            var payment = new Payment(idGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(idGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111", "01",
                                              "2020", "Card Holder Name");
            var model = new CardPayment(idGenerator.GenerateId(), paymentCard, payment, "123");

            var data = ctx.Mapper.Map<CardPaymentData>(model);

            data.Should().BeEquivalentTo(model);

            data.Id.Should().Be(model.Id);

//            Assert.AreEqual(cardPayment.Id,                    data.Id);
//            Assert.AreEqual(cardPayment.CardVerificationValue, data.CardVerificationValue);
//            Assert.AreEqual(cardPayment.Payment.Id,            data.Payment.Id);
//            Assert.AreEqual(cardPayment.PaymentCard.Id,        data.PaymentCard.Id);

            model = ctx.Mapper.Map<CardPayment>(data);

            model.Id.Should().Be(data.Id);

//            Assert.IsNotNull(cardPayment);
//            Assert.AreEqual(data.Id,                    cardPayment.Id);
//            Assert.AreEqual(data.CardVerificationValue, cardPayment.CardVerificationValue);
//            Assert.AreEqual(data.Payment.Id,            cardPayment.Payment.Id);
//            Assert.AreEqual(data.PaymentCard.Id,        cardPayment.PaymentCard.Id);
        }

        [Fact]
        public void WhenMappingPayment_ShouldMapBothWays()
        {
            var ctx = new TestContext();
            var model = new Payment("1", 1234.50m, "GBP", "Test");
            var data = ctx.Mapper.Map<PaymentData>(model);

//            Assert.IsNotNull(data);
//            Assert.AreEqual(model.Id,          data.Id);
//            Assert.AreEqual(model.Amount,      data.Amount);
//            Assert.AreEqual(model.Currency,    data.Currency);
//            Assert.AreEqual(model.Description, data.Description);

            data.Should().BeEquivalentTo(model);

            model = ctx.Mapper.Map<Payment>(data);

            model.Id.Should().Be(data.Id);

//            Assert.IsNotNull(model);
//            Assert.AreEqual(data.Id,          model.Id);
//            Assert.AreEqual(data.Amount,      model.Amount);
//            Assert.AreEqual(data.Currency,    model.Currency);
//            Assert.AreEqual(data.Description, model.Description);
        }

        [Fact]
        public void WhenMappingPaymentCard_ShouldMapBothWays()
        {
            var ctx = new TestContext();
            var model = new PaymentCard("1", "Card Alias", "Visa", "4111111111111111", "01", "2020",
                                        "Card Holder Name");
            var data = ctx.Mapper.Map<PaymentCardData>(model);

//            Assert.IsNotNull(data);
//            Assert.AreEqual(model.Id,             data.Id);
//            Assert.AreEqual(model.Alias,          data.Alias);
//            Assert.AreEqual(model.CardNumber,     data.CardNumber);
//            Assert.AreEqual(model.Type,           data.Type);
//            Assert.AreEqual(model.ExpiryMonth,    data.ExpiryMonth);
//            Assert.AreEqual(model.ExpiryYear,     data.ExpiryYear);
//            Assert.AreEqual(model.CardHolderName, data.CardHolderName);

            data.Should().BeEquivalentTo(model);

            model = ctx.Mapper.Map<PaymentCard>(data);

            model.Id.Should().Be(data.Id);

//            Assert.IsNotNull(model);
//            Assert.AreEqual(data.Id,             model.Id);
//            Assert.AreEqual(data.Alias,          model.Alias);
//            Assert.AreEqual(data.CardNumber,     model.CardNumber);
//            Assert.AreEqual(data.Type,           model.Type);
//            Assert.AreEqual(data.ExpiryMonth,    model.ExpiryMonth);
//            Assert.AreEqual(data.ExpiryYear,     model.ExpiryYear);
//            Assert.AreEqual(data.CardHolderName, model.CardHolderName);
        }
    }
}