using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class RepositoryTests
    {
        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        public async Task WhenDeletingItem_ShouldSucceed(RepositoryTestContext ctx)
        {
            var model = new CardPayment
            {
                Id = ctx.IdGenerator.GenerateId(),
                CardVerificationValue = "123",
                Payment = new Payment
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Amount = 12345m,
                    Currency = "GBP",
                    Description = "Description"
                },
                PaymentCard = new PaymentCard
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Alias = "Alias",
                    Type = "Type",
                    CardNumber = "CardNumber",
                    ExpiryMonth = "ExpiryMonth",
                    ExpiryYear = "ExpiryYear",
                    CardHolderName = "CardHolderName"
                }
            };

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repository.CreateAsync(model);
                await repository.SaveChangesAsync();
            }

            IEnumerable<CardPayment> beforeDeleteModel;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                beforeDeleteModel = await repository.GetAllAsync();
            }

            var beforeDeleteModelList = beforeDeleteModel.ToList();

            IEnumerable<CardPayment> afterDeleteModel;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repository.DeleteAsync(model);
                await repository.SaveChangesAsync();
            }

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                afterDeleteModel = await repository.GetAllAsync();
            }

            var afterDeleteModelList = afterDeleteModel.ToList();

            beforeDeleteModelList.Count.Should().Be(1);
            afterDeleteModelList.Count.Should().Be(0);
        }

        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        public async Task WhenFindingItemUsingPredicate_ShouldSucceed(RepositoryTestContext ctx)
        {
            var model = new CardPayment
            {
                Id = ctx.IdGenerator.GenerateId(),
                CardVerificationValue = "123",
                Payment = new Payment
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Amount = 12345m,
                    Currency = "GBP",
                    Description = "Description"
                },
                PaymentCard = new PaymentCard
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Alias = "Alias",
                    Type = "Type",
                    CardNumber = "CardNumber",
                    ExpiryMonth = "ExpiryMonth",
                    ExpiryYear = "ExpiryYear",
                    CardHolderName = "CardHolderName"
                }
            };

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repository.CreateAsync(model);
                await repository.SaveChangesAsync();
            }

            IEnumerable<CardPayment> results;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                results = await repository.WhereAsync(x => x.Payment.Id == model.Payment.Id);
            }

            var resultsList = results.ToList();

            resultsList.Count.Should().Be(1);
            resultsList[0].Payment.Id.Should().Be(model.Payment.Id);
        }

        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        public async Task WhenGettingAllItems_ShouldSucceed(RepositoryTestContext ctx)
        {
            var model = new CardPayment
            {
                Id = ctx.IdGenerator.GenerateId(),
                CardVerificationValue = "123",
                Payment = new Payment
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Amount = 12345m,
                    Currency = "GBP",
                    Description = "Description"
                },
                PaymentCard = new PaymentCard
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Alias = "Alias",
                    Type = "Type",
                    CardNumber = "CardNumber",
                    ExpiryMonth = "ExpiryMonth",
                    ExpiryYear = "ExpiryYear",
                    CardHolderName = "CardHolderName"
                }
            };

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repository.CreateAsync(model);
                await repository.SaveChangesAsync();
            }

            IEnumerable<CardPayment> savedModels;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                savedModels = await repository.GetAllAsync();
            }

            var savedModelsList = savedModels.ToList();

            savedModelsList.Count.Should().Be(1);
            savedModelsList[0].Id.Should().Be(model.Id);
            savedModelsList[0].Payment.Id.Should().Be(model.Payment.Id);
            savedModelsList[0].PaymentCard.Id.Should().Be(model.PaymentCard.Id);
        }

        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        public async Task WhenSavingItemAndGettingItem_ShouldSucceed(RepositoryTestContext ctx)
        {
            var model = new CardPayment
            {
                Id = ctx.IdGenerator.GenerateId(),
                CardVerificationValue = "123",
                Payment = new Payment
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Amount = 12345m,
                    Currency = "GBP",
                    Description = "Description"
                },
                PaymentCard = new PaymentCard
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Alias = "Alias",
                    Type = "Type",
                    CardNumber = "CardNumber",
                    ExpiryMonth = "ExpiryMonth",
                    ExpiryYear = "ExpiryYear",
                    CardHolderName = "CardHolderName"
                }
            };

            string id;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                id = await repository.CreateAsync(model);
                await repository.SaveChangesAsync();
            }

            CardPayment savedModel;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                savedModel = await repository.GetAsync(id);
            }

            savedModel.Id.Should().Be(model.Id);
            savedModel.Payment.Id.Should().Be(model.Payment.Id);
            savedModel.PaymentCard.Id.Should().Be(model.PaymentCard.Id);
        }

        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        public async Task WhenUpdatingItem_ShouldSucceed(RepositoryTestContext ctx)
        {
            var model = new CardPayment
            {
                Id = ctx.IdGenerator.GenerateId(),
                CardVerificationValue = "123",
                Payment = new Payment
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Amount = 12345m,
                    Currency = "GBP",
                    Description = "Description"
                },
                PaymentCard = new PaymentCard
                {
                    Id = ctx.IdGenerator.GenerateId(),
                    Alias = "Alias",
                    Type = "Type",
                    CardNumber = "CardNumber",
                    ExpiryMonth = "ExpiryMonth",
                    ExpiryYear = "ExpiryYear",
                    CardHolderName = "CardHolderName"
                }
            };

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repository.CreateAsync(model);
                await repository.SaveChangesAsync();
            }

            model.Payment.Amount = 999999m;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repository.UpdateAsync(model);
                await repository.SaveChangesAsync();
            }

            CardPayment updatedItem;

            using (var repository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                updatedItem = await repository.GetAsync(model.Id);
            }

            updatedItem.Payment.Amount.Should().Be(model.Payment.Amount);
        }
    }
}