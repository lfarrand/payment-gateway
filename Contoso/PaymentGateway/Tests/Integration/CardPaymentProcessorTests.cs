using System;
using Contoso.PaymentGateway.BusinessLogic.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using FluentAssertions;
using Moq;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class CardPaymentProcessorTests
    {
        [Fact]
        public void WhenCvvIncorrect_PaymentShouldFail()
        {
            var ctx = new CardPaymentProcessorTestContext();
            var cardIssuerApi = Mock.Of<ICardIssuerApi>(x =>
                                                            x.ChargeCard(It.IsAny<PaymentCard>(), It.IsAny<Payment>()) ==
                                                            new ChargeCardResult(ctx.IdGenerator.GenerateId(), ChargeCardOperationStatus.DeclinedCvvIncorrect));

            ICardPaymentProcessor cardPaymentProcessor =
                new CardPaymentProcessor(ctx.Log, ctx.DateTimeProvider, cardIssuerApi, ctx.StatusCodeMapping);

            var payment = new Payment(ctx.IdGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(ctx.IdGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111",
                                              "01",
                                              "2020", "Card Holder Name");

            var result = cardPaymentProcessor.ProcessPayment(paymentCard, payment);

            result.Success.Should().BeFalse();
            result.StatusCode.Should().Be(ProcessPaymentResultStatusCode.Failed);
        }

        [Fact]
        public void WhenExceptionThrown_PaymentShouldFail()
        {
            var ctx = new CardPaymentProcessorTestContext();
            var cardIssuerApiMock = new Mock<ICardIssuerApi>();

            cardIssuerApiMock.Setup(item => item.ChargeCard(It.IsAny<PaymentCard>(), It.IsAny<Payment>()))
                             .Throws(new Exception());

            ICardPaymentProcessor cardPaymentProcessor =
                new CardPaymentProcessor(ctx.Log, ctx.DateTimeProvider, cardIssuerApiMock.Object,
                                         ctx.StatusCodeMapping);

            var payment = new Payment(ctx.IdGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(ctx.IdGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111",
                                              "01",
                                              "2020", "Card Holder Name");

            var result = cardPaymentProcessor.ProcessPayment(paymentCard, payment);

            result.Success.Should().BeFalse();
            result.StatusCode.Should().Be(ProcessPaymentResultStatusCode.Failed);
        }

        [Fact]
        public void WhenNoMappingForStatusCode_PaymentShouldFail()
        {
            var ctx = new CardPaymentProcessorTestContext();
            var cardIssuerApi = Mock.Of<ICardIssuerApi>(x =>
                                                            x.ChargeCard(It.IsAny<PaymentCard>(), It.IsAny<Payment>()) ==
                                                            new ChargeCardResult(ctx.IdGenerator.GenerateId(), ChargeCardOperationStatus.Unknown));

            ICardPaymentProcessor cardPaymentProcessor =
                new CardPaymentProcessor(ctx.Log, ctx.DateTimeProvider, cardIssuerApi, ctx.StatusCodeMapping);

            var payment = new Payment(ctx.IdGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(ctx.IdGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111",
                                              "01",
                                              "2020", "Card Holder Name");

            var result = cardPaymentProcessor.ProcessPayment(paymentCard, payment);

            result.Success.Should().BeFalse();
            result.StatusCode.Should().Be(ProcessPaymentResultStatusCode.Failed);
        }

        [Fact]
        public void WhenNotEnoughFunds_PaymentShouldFail()
        {
            var ctx = new CardPaymentProcessorTestContext();
            var cardIssuerApi = Mock.Of<ICardIssuerApi>(x =>
                                                            x.ChargeCard(It.IsAny<PaymentCard>(), It.IsAny<Payment>()) ==
                                                            new ChargeCardResult(ctx.IdGenerator.GenerateId(), ChargeCardOperationStatus.DeclinedNotEnoughFunds));

            ICardPaymentProcessor cardPaymentProcessor =
                new CardPaymentProcessor(ctx.Log, ctx.DateTimeProvider, cardIssuerApi, ctx.StatusCodeMapping);

            var payment = new Payment(ctx.IdGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(ctx.IdGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111",
                                              "01",
                                              "2020", "Card Holder Name");

            var result = cardPaymentProcessor.ProcessPayment(paymentCard, payment);

            result.Success.Should().BeFalse();
            result.StatusCode.Should().Be(ProcessPaymentResultStatusCode.Failed);
        }
    }
}