using System.Threading.Tasks;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using Contoso.PaymentGateway.Utilities.Identity;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class CardPaymentOrchestratorTests
    {
        [Theory]
        [ClassData(typeof(CardPaymentOrchestratorTestCases))]
        public async Task WhenGettingCardPayment_CardNumberShouldBeMasked(CardPaymentOrchestratorTestContext ctx)
        {
            var idGenerator = new StringGuidIdentityGenerator(new GuidIdentityGenerator());
            var payment = new Payment(idGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(idGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111", "01",
                                              "2020", "Card Holder Name");
            var cardPayment = new CardPayment(idGenerator.GenerateId(), paymentCard, payment, "123");

            using (var cardPaymentRepository = ctx.RepositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await cardPaymentRepository.CreateAsync(cardPayment);
                await cardPaymentRepository.SaveChangesAsync();
            }

            var request = new GetCardPaymentRequest {CardPaymentId = cardPayment.Id};
            var response = await ctx.CardPaymentOrchestrator.GetCardPayment(request);

            response.Success.Should().BeTrue();
            response.CardPayment.PaymentCard.CardNumber.Should().Be("XXXXXXXXXXXX1111");
        }

        [Theory]
        [ClassData(typeof(CardPaymentOrchestratorTestCases))]
        public async Task WhenMakingCardPayment_PaymentShouldSucceed(CardPaymentOrchestratorTestContext ctx)
        {
            var idGenerator = new StringGuidIdentityGenerator(new GuidIdentityGenerator());
            var payment = new Payment(idGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");
            var paymentCard = new PaymentCard(idGenerator.GenerateId(), "Card Alias", "Visa", "4111111111111111", "01",
                                              "2020", "Card Holder Name");

            using (var paymentCardRepository = ctx.RepositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                await paymentCardRepository.CreateAsync(paymentCard);
                await paymentCardRepository.SaveChangesAsync();
            }

            var request = new MakeCardPaymentRequest {Payment = payment, PaymentCardId = paymentCard.Id};
            var response = await ctx.CardPaymentOrchestrator.MakeCardPayment(request);

            response.Success.Should().BeTrue();
            response.ResponseCode.Should().Be(MakePaymentResponseCode.Success);
            response.PaymentId.Should().Be(payment.Id);
        }

        [Theory]
        [ClassData(typeof(CardPaymentOrchestratorTestCases))]
        public async Task WhenNoCardExists_CardPaymentShouldFail(CardPaymentOrchestratorTestContext ctx)
        {
            var idGenerator = new StringGuidIdentityGenerator(new GuidIdentityGenerator());
            var payment = new Payment(idGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");

            var request = new MakeCardPaymentRequest {Payment = payment, PaymentCardId = idGenerator.GenerateId()};
            var response = await ctx.CardPaymentOrchestrator.MakeCardPayment(request);

            response.Success.Should().BeFalse();
            response.ResponseCode.Should().Be(MakePaymentResponseCode.FailedPaymentCardNotFound);
        }
    }
}