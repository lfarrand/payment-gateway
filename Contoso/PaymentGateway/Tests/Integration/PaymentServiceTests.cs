using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.BusinessLogic.Orchestrator;
using Contoso.PaymentGateway.DataAccess.DataStore;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using FluentAssertions;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class PaymentOrchestratorTests
    {
        [Theory]
        [ClassData(typeof(RepositoryFactoryTestCases))]
        private async Task WhenGettingExistingPayment_ShouldReturnCorrectPayment(RepositoryTestContext ctx)
        {
            // Arrange
            IDataStore<Payment> paymentDataStore = new PaymentDataStore(ctx.RepositoryFactory);

            IPaymentOrchestrator paymentOrchestrator = new PaymentOrchestrator(ctx.Log, paymentDataStore);
            var payment = new Payment(ctx.IdGenerator.GenerateId(), 1234.50m, "GBP", "Test payment");

            using (var repository = ctx.RepositoryFactory.Create<Payment, PaymentData, string>())
            {
                await repository.CreateAsync(payment);
            }

            var request = new GetPaymentRequest {PaymentId = payment.Id};

            // Act
            var response = await paymentOrchestrator.GetPayment(request);

            // Assert
            response.Success.Should().BeTrue();
        }
    }
}