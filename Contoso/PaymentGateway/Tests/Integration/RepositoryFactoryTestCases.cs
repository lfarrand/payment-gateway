using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class RepositoryFactoryTestCases : TheoryData<TestContext>
    {
        public RepositoryFactoryTestCases()
        {
            Add(new InMemoryRepositoryTestContext());
            Add(new EntityFrameworkRepositoryTestContext());
        }
    }
}