using Contoso.PaymentGateway.Tests.Integration.TestContexts;
using Xunit;

namespace Contoso.PaymentGateway.Tests.Integration
{
    public class CardPaymentOrchestratorTestCases : TheoryData<CardPaymentOrchestratorTestContext>
    {
        public CardPaymentOrchestratorTestCases()
        {
            Add(new CardPaymentOrchestratorTestContext(new InMemoryRepositoryTestContext().RepositoryFactory));
            Add(new CardPaymentOrchestratorTestContext(new EntityFrameworkRepositoryTestContext().RepositoryFactory));
        }
    }
}