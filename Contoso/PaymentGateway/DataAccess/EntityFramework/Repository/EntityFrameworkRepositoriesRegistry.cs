using System;
using System.Collections.Generic;
using AutoMapper;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Utilities.Construction;
using Contoso.PaymentGateway.Utilities.Interfaces.Construction;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.Repository
{
    public class
        EntityFrameworkRepositoriesRegistry : IFactory<Dictionary<Type, IFactory<DbContext, IMapper, IRepository>>>
    {
        public Dictionary<Type, IFactory<DbContext, IMapper, IRepository>> Create()
        {
            var repositoryFactories = new Dictionary<Type, IFactory<DbContext, IMapper, IRepository>>
            {
                [typeof(IRepository<PaymentCard, string>)] =
                    new Factory<DbContext, IMapper, IRepository>((ctx, map) =>
                                                                     new PaymentCardEntityFrameworkRepository(ctx, map)),
                [typeof(IRepository<CardPayment, string>)] =
                    new Factory<DbContext, IMapper, IRepository>((ctx, map) =>
                                                                     new CardPaymentEntityFrameworkRepository(ctx, map))
            };

            return repositoryFactories;
        }
    }
}