using System;
using System.Collections.Generic;
using AutoMapper;
using Contoso.PaymentGateway.DataAccess.EntityFramework.Interfaces.DatabaseContext;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.Models.Interfaces.Entity;
using Contoso.PaymentGateway.Utilities.Interfaces.Construction;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.Repository
{
    public class EntityFrameworkRepositoryFactory : IRepositoryFactory
    {
        private readonly IDbContextFactory dbContextFactory;
        private readonly IMapper mapper;
        private readonly Dictionary<Type, IFactory<DbContext, IMapper, IRepository>> repositoryFactories;

        public EntityFrameworkRepositoryFactory(IMapper mapper, IDbContextFactory dbContextFactory,
                                                Dictionary<Type, IFactory<DbContext, IMapper, IRepository>> repositoryFactories)
        {
            this.mapper = mapper;
            this.dbContextFactory = dbContextFactory;
            this.repositoryFactories = repositoryFactories;
        }

        public IRepository<TModel, TKey> Create<TModel, TData, TKey>() where TModel : class, IEntity<TKey>, new()
                                                                       where TData : class, IEntity<TKey>, new()
        {
            var context = dbContextFactory.CreateContext();
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            if (repositoryFactories.TryGetValue(typeof(IRepository<TModel, TKey>), out var factory))
            {
                var repository = factory.Create(context, mapper);
                return (IRepository<TModel, TKey>) repository;
            }

            return new EntityFrameworkRepository<TModel, TData, TKey>(context, mapper);
        }

        public void Dispose()
        {
            dbContextFactory?.Dispose();
        }
    }
}