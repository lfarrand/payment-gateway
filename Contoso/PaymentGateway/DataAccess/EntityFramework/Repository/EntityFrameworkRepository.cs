﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.Models.Interfaces.Entity;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.Repository
{
    public class EntityFrameworkRepository<TModel, TData, TKey> : IRepository<TModel, TKey>
        where TModel : class, IEntity<TKey>, new() where TData : class, IEntity<TKey>, new()
    {
        protected readonly DbContext DbContext;
        protected readonly IMapper Mapper;

        public EntityFrameworkRepository(DbContext dbContext, IMapper mapper)
        {
            DbContext = dbContext;
            Mapper = mapper;
        }

        public virtual async Task<TModel> GetAsync(TKey id)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            var item = await DbContext.Set<TData>()
                                      .FirstOrDefaultAsync(x => Equals(x.Id, id));

            return item != null ? Mapper.Map<TModel>(item) : null;
        }

        public virtual async Task<IList<TModel>> GetAllAsync()
        {
            return await DbContext.Set<TData>()
                                  .Select(x => Mapper.Map<TModel>(x))
                                  .ToListAsync();
        }

        public virtual async Task SaveChangesAsync()
        {
            await DbContext.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(TModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            await Task.Run(() =>
            {
                var data = new TData {Id = model.Id};
                DbContext.Entry(data).State = EntityState.Deleted;
            });
        }

        public virtual async Task<IList<TModel>> WhereAsync(Expression<Func<TModel, bool>> predicate)
        {
            return await DbContext.Set<TData>()
                                  .UseAsDataSource(Mapper).For<TModel>()
                                  .Where(predicate)
                                  .Select(x => Mapper.Map<TModel>(x))
                                  .ToListAsync();
        }

        public void Dispose()
        {
            DbContext?.Dispose();
        }

        public virtual async Task<TKey> CreateAsync(TModel entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var data = Mapper.Map<TData>(entity);
            await DbContext.Set<TData>().AddAsync(data);
            return data.Id;
        }

        public virtual async Task UpdateAsync(TModel entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            await Task.Run(() => { UpdateDatabaseValues<TModel, TData, TKey>(entity); });
        }

        protected void UpdateDatabaseValues<T1, T2, T3>(T1 entity) where T1 : class, IEntity<T3>
                                                                   where T2 : class, IEntity<T3>
        {
            var data = Mapper.Map<T2>(entity);
            DbContext.Entry(data).CurrentValues.SetValues(data);
            DbContext.Entry(data).State = EntityState.Modified;
        }
    }
}