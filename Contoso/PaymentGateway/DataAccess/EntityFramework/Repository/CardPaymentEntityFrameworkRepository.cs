using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.Repository
{
    public class CardPaymentEntityFrameworkRepository : EntityFrameworkRepository<CardPayment, CardPaymentData, string>
    {
        public CardPaymentEntityFrameworkRepository(DbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        public override async Task<CardPayment> GetAsync(string id)
        {
            var item = await DbContext.Set<CardPaymentData>()
                                      .Include(x => x.Payment)
                                      .Include(x => x.PaymentCard)
                                      .FirstOrDefaultAsync(x => x.Id == id);

            return item != null ? Mapper.Map<CardPayment>(item) : null;
        }

        public override async Task<IList<CardPayment>> GetAllAsync()
        {
            return await DbContext.Set<CardPaymentData>()
                                  .Include(x => x.Payment)
                                  .Include(x => x.PaymentCard)
                                  .Select(x => Mapper.Map<CardPayment>(x))
                                  .ToListAsync();
        }

        public override async Task<IList<CardPayment>> WhereAsync(Expression<Func<CardPayment, bool>> predicate)
        {
            return await Task.Run(() =>
            {
                return DbContext.Set<CardPaymentData>()
                                .Include(x => x.Payment)
                                .Include(x => x.PaymentCard)
                                .UseAsDataSource(Mapper).For<CardPayment>()
                                .Where(predicate)
                                .ToList();
            });
        }

        public override async Task UpdateAsync(CardPayment entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            await Task.Run(() =>
            {
                if (entity.Payment != null)
                {
                    UpdateDatabaseValues<Payment, PaymentData, string>(entity.Payment);
                }

                if (entity.PaymentCard != null)
                {
                    UpdateDatabaseValues<PaymentCard, PaymentCardData, string>(entity.PaymentCard);
                }

                UpdateDatabaseValues<CardPayment, CardPaymentData, string>(entity);
            });
        }
    }
}