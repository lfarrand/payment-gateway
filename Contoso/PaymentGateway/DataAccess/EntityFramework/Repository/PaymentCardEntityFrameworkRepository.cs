using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.Repository
{
    public class PaymentCardEntityFrameworkRepository : EntityFrameworkRepository<PaymentCard, PaymentCardData, string>
    {
        public PaymentCardEntityFrameworkRepository(DbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        public override async Task<PaymentCard> GetAsync(string id)
        {
            var item = await DbContext.Set<PaymentCardData>()
                                      .Include(x => x.Payments)
                                      .Include(x => x.CardPayments)
                                      .FirstOrDefaultAsync(x => x.Id == id);

            return item != null ? Mapper.Map<PaymentCard>(item) : null;
        }

        public override async Task<IList<PaymentCard>> GetAllAsync()
        {
            return await DbContext.Set<PaymentCardData>()
                                  .Include(x => x.Payments)
                                  .Include(x => x.CardPayments)
                                  .Select(x => Mapper.Map<PaymentCard>(x))
                                  .ToListAsync();
        }

        public override async Task<IList<PaymentCard>> WhereAsync(Expression<Func<PaymentCard, bool>> predicate)
        {
            return await DbContext.Set<PaymentCardData>()
                                  .Include(x => x.Payments)
                                  .Include(x => x.CardPayments)
                                  .UseAsDataSource(Mapper).For<PaymentCard>()
                                  .Where(predicate)
                                  .Select(x => Mapper.Map<PaymentCard>(x))
                                  .ToListAsync();
        }

        public override async Task UpdateAsync(PaymentCard entity)
        {
            var data = Mapper.Map<PaymentCardData>(entity);

            await DbContext.ReconcileAsync(data, e => e
                                                     .WithMany(p => p.Payments)
                                                     .WithMany(p => p.CardPayments));

            await DbContext.SaveChangesAsync();
        }
    }
}