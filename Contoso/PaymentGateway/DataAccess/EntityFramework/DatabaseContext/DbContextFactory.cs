using Contoso.PaymentGateway.DataAccess.EntityFramework.Interfaces.DatabaseContext;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.DatabaseContext
{
    public abstract class DbContextFactory<T> : IDbContextFactory where T : DbContext
    {
        protected DbContextFactory(DbContextOptionsBuilder<T> optionsBuilder)
        {
            OptionsBuilder = optionsBuilder;
        }

        protected DbContextFactory() : this(new DbContextOptionsBuilder<T>())
        {
        }

        protected DbContextOptionsBuilder<T> OptionsBuilder { get; }

        protected DbContextOptions<T> Options { get; set; }

        public abstract void Dispose();

        public abstract DbContext CreateContext();
    }
}