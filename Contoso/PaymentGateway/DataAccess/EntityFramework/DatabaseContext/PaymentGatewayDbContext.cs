using Contoso.PaymentGateway.DataAccess.Model;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.DatabaseContext
{
    public class PaymentGatewayDbContext : DbContext
    {
        public PaymentGatewayDbContext(DbContextOptions<PaymentGatewayDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<CardPaymentData>().ToTable("CardPayment");
            builder.Entity<CardPaymentData>().HasKey(p => p.Id);
            builder.Entity<CardPaymentData>().Property(p => p.Id).IsRequired();
            builder.Entity<CardPaymentData>().Property(p => p.CardVerificationValue).IsRequired().HasMaxLength(3);
            builder.Entity<CardPaymentData>().HasOne(p => p.Payment);
            builder.Entity<CardPaymentData>().HasOne(p => p.PaymentCard);

            builder.Entity<PaymentCardData>().ToTable("PaymentCard");
            builder.Entity<PaymentCardData>().HasKey(p => p.Id);
            builder.Entity<PaymentCardData>().Property(p => p.Id).IsRequired();
            builder.Entity<PaymentCardData>().Property(p => p.Alias).IsRequired().HasMaxLength(50);
            builder.Entity<PaymentCardData>().Property(p => p.Type).IsRequired().HasMaxLength(50);
            builder.Entity<PaymentCardData>().Property(p => p.CardNumber).IsRequired().HasMaxLength(16);
            builder.Entity<PaymentCardData>().Property(p => p.ExpiryMonth).IsRequired().HasMaxLength(2);
            builder.Entity<PaymentCardData>().Property(p => p.ExpiryYear).IsRequired().HasMaxLength(4);
            builder.Entity<PaymentCardData>().Property(p => p.CardHolderName).IsRequired().HasMaxLength(50);
            builder.Entity<PaymentCardData>().HasMany(a => a.CardPayments);
            builder.Entity<PaymentCardData>().HasMany(a => a.Payments);

            builder.Entity<PaymentData>().ToTable("Payment");
            builder.Entity<PaymentData>().HasKey(p => p.Id);
            builder.Entity<PaymentData>().Property(p => p.Id).IsRequired();
            builder.Entity<PaymentData>().Property(p => p.Amount).IsRequired();
            builder.Entity<PaymentData>().Property(p => p.Currency).IsRequired().HasMaxLength(3);
            builder.Entity<PaymentData>().Property(p => p.Description).IsRequired().HasMaxLength(50);
        }
    }
}