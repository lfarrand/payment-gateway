using System.Data.Common;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.DatabaseContext
{
    public class SqlLitePaymentGatewayDbContextFactory : DbContextFactory<PaymentGatewayDbContext>
    {
        private DbConnection connection;

        public SqlLitePaymentGatewayDbContextFactory()
        {
        }

        public SqlLitePaymentGatewayDbContextFactory(DbContextOptionsBuilder<PaymentGatewayDbContext> optionsBuilder) :
            base(optionsBuilder)
        {
        }

        public override DbContext CreateContext()
        {
            if (connection != null)
            {
                return new PaymentGatewayDbContext(Options);
            }

            connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            Options = OptionsBuilder.UseSqlite(connection).Options;

            using (var context = new PaymentGatewayDbContext(Options))
            {
                // Create schema in database
                context.Database.EnsureCreated();
            }

            return new PaymentGatewayDbContext(Options);
        }

        public override void Dispose()
        {
            connection?.Close();
            connection?.Dispose();
        }
    }
}