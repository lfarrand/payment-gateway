using System;
using Microsoft.EntityFrameworkCore;

namespace Contoso.PaymentGateway.DataAccess.EntityFramework.Interfaces.DatabaseContext
{
    public interface IDbContextFactory : IDisposable
    {
        DbContext CreateContext();
    }
}