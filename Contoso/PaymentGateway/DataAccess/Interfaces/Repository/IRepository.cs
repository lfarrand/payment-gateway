using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.DataAccess.Interfaces.Repository
{
    public interface IRepository : IDisposable
    {
    }

    public interface IRepository<TItem, TKey> : IRepository where TItem : IEntity<TKey>
    {
        Task<TItem> GetAsync(TKey id);

        Task<IList<TItem>> GetAllAsync();

        Task SaveChangesAsync();

        Task<TKey> CreateAsync(TItem entity);

        Task UpdateAsync(TItem entity);

        Task DeleteAsync(TItem model);

        Task<IList<TItem>> WhereAsync(Expression<Func<TItem, bool>> predicate);
    }
}