using System;
using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.DataAccess.Interfaces.Repository
{
    public interface IRepositoryFactory : IDisposable
    {
        IRepository<TModel, TKey> Create<TModel, TData, TKey>() where TModel : class, IEntity<TKey>, new()
                                                                where TData : class, IEntity<TKey>, new();
    }
}