using System;
using System.Threading.Tasks;

namespace Contoso.PaymentGateway.DataAccess.Interfaces.Concurrency
{
    public interface ILockManager
    {
        Task<ILockDisposable> GetLockAsync(string key);

        Task ReleaseLockAsync(Guid uniqueId);
    }
}