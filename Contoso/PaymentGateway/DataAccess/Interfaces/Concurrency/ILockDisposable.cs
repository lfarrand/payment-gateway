using System;

namespace Contoso.PaymentGateway.DataAccess.Interfaces.Concurrency
{
    public interface ILockDisposable : IDisposable
    {
    }
}