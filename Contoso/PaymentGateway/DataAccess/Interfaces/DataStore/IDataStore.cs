using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Contoso.PaymentGateway.DataAccess.Interfaces.DataStore
{
    public interface IDataStore<T>
    {
        Task<T> GetAsync(string id);

        Task<IList<T>> GetAllAsync();

        Task SaveChangesAsync();

        Task<string> CreateAsync(T T);

        Task UpdateAsync(T T);

        Task DeleteAsync(T T);

        Task<IList<T>> WhereAsync(Expression<Func<T, bool>> predicate);
    }
}