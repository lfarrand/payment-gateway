using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contoso.PaymentGateway.DataAccess.Interfaces.Concurrency;

namespace Contoso.PaymentGateway.DataAccess.Concurrency
{
    public class LockManager : ILockManager
    {
        private readonly TimeSpan defaultLockDuration = TimeSpan.FromSeconds(5);
        private readonly TimeSpan defaultRetryInterval = TimeSpan.FromSeconds(1);
        private readonly Dictionary<string, Lock> locksByKey = new Dictionary<string, Lock>();
        private readonly Dictionary<Guid, Lock> locksByUniqueId = new Dictionary<Guid, Lock>();
        private readonly TimeSpan maxRetryPeriod = TimeSpan.FromSeconds(30);
        private readonly object padLock = new object();

        public async Task<ILockDisposable> GetLockAsync(string key)
        {
            var getLockCalledAt = DateTime.UtcNow;
            var retryTimeoutPoint = getLockCalledAt.Add(maxRetryPeriod);

            while (retryTimeoutPoint > DateTime.UtcNow)
            {
                lock (padLock)
                {
                    if (locksByKey.TryGetValue(key, out var lck))
                    {
                        if (lck.ExpiresAt < DateTime.UtcNow)
                        {
                            // Lock has expired so remove it
                            locksByKey.Remove(lck.Key);
                            locksByUniqueId.Remove(lck.UniqueId);
                        }
                    }
                    else
                    {
                        // We successfully acquired the lock
                        lck = new Lock(key, DateTime.UtcNow.Add(defaultLockDuration));
                        locksByKey[lck.Key] = lck;
                        locksByUniqueId[lck.UniqueId] = lck;
                        return new LockDisposable(this, lck, true);
                    }
                }

                // Lets wait and retry
                await Task.Delay(defaultRetryInterval);
            }

            // Timeout expired, we haven't acquired the lock in the allotted time
            return new LockDisposable(this);
        }

        public async Task ReleaseLockAsync(Guid uniqueId)
        {
            await Task.Run(
                           () =>
                           {
                               lock (padLock)
                               {
                                   // Remove the lock from both indexes
                                   locksByUniqueId.Remove(uniqueId, out var lck);
                                   locksByKey.Remove(lck.Key);
                               }
                           });
        }
    }
}