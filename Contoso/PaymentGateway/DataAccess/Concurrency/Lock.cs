using System;

namespace Contoso.PaymentGateway.DataAccess.Concurrency
{
    public class Lock
    {
        public Lock(string key, DateTime expiresAt)
        {
            Key = key;
            ExpiresAt = expiresAt;
            UniqueId = Guid.NewGuid();
        }

        public Guid UniqueId { get; }

        public string Key { get; }

        public DateTime ExpiresAt { get; }
    }
}