using Contoso.PaymentGateway.DataAccess.Interfaces.Concurrency;

namespace Contoso.PaymentGateway.DataAccess.Concurrency
{
    public class LockDisposable : ILockDisposable
    {
        private readonly Lock lck;
        private readonly ILockManager lockManager;

        public LockDisposable(ILockManager lockManager, Lock lck = null, bool wasLockAcquired = false)
        {
            WasLockAcquired = wasLockAcquired;
            this.lockManager = lockManager;
            this.lck = lck;
        }

        public bool WasLockAcquired { get; }

        public void Dispose()
        {
            lockManager.ReleaseLockAsync(lck.UniqueId).Wait();
        }
    }
}