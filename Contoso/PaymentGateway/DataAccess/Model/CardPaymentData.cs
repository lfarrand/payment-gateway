using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.DataAccess.Model
{
    public class CardPaymentData : IEntity<string>
    {
        public PaymentCardData PaymentCard { get; set; }

        public PaymentData Payment { get; set; }

        public string CardVerificationValue { get; set; }

        public string Id { get; set; }
    }
}