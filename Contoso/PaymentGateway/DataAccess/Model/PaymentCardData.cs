using System.Collections.Generic;
using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.DataAccess.Model
{
    public class PaymentCardData : IEntity<string>
    {
        public string Alias { get; set; }

        /// <summary>
        ///     The type of card: Visa, MasterCard etc.
        /// </summary>
        public string Type { get; set; }

        public string CardNumber { get; set; }

        public string ExpiryMonth { get; set; }

        public string ExpiryYear { get; set; }

        public string CardHolderName { get; set; }

        public List<PaymentData> Payments { get; set; }

        public List<CardPaymentData> CardPayments { get; set; }

        public string Id { get; set; }
    }
}