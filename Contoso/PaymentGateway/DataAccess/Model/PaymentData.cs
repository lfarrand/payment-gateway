using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.DataAccess.Model
{
    public class PaymentData : IEntity<string>
    {
        public decimal Amount { get; set; }

        public string Currency { get; set; }

        public string Description { get; set; }

        public string Id { get; set; }
    }
}