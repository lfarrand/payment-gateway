using System;
using System.Collections.Generic;
using AutoMapper;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.DataAccess.Repository
{
    public class InMemoryRepositoryFactory : IRepositoryFactory
    {
        private readonly IMapper mapper;
        private readonly Dictionary<Type, IRepository> repositories = new Dictionary<Type, IRepository>();

        public InMemoryRepositoryFactory(IMapper mapper)
        {
            this.mapper = mapper;
        }

        /// <summary>
        ///     This method creates instances of <c>InMemoryRepository</c> and caches
        ///     them in memory. This allows state to be shared amongst multiple steps
        ///     in a pipeline.
        /// </summary>
        /// <typeparam name="TModel">The domain model type</typeparam>
        /// <typeparam name="TData">The data transfer type</typeparam>
        /// <typeparam name="TKey">The primary key type</typeparam>
        /// <returns>Instance of <c>InMemoryRepository</c></returns>
        public IRepository<TModel, TKey> Create<TModel, TData, TKey>() where TModel : class, IEntity<TKey>, new()
                                                                       where TData : class, IEntity<TKey>, new()
        {
            if (!repositories.TryGetValue(typeof(IRepository<TModel, TKey>), out var obj))
            {
                var repository = new InMemoryRepository<TModel, TData, TKey>(mapper);
                repositories[typeof(IRepository<TModel, TKey>)] = repository;
                return repository;
            }

            return (IRepository<TModel, TKey>) obj;
        }

        public void Dispose()
        {
        }
    }
}