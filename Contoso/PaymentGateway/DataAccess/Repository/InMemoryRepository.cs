using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.Models.Interfaces.Entity;

namespace Contoso.PaymentGateway.DataAccess.Repository
{
    /// <summary>
    ///     This class is an in memory repository, designed to be used for testing.
    ///     This is not designed to be used in a live application.
    /// </summary>
    /// <typeparam name="TModel">The domain model type</typeparam>
    /// <typeparam name="TData">The data transfer type</typeparam>
    /// <typeparam name="TKey">The primary key type</typeparam>
    public class InMemoryRepository<TModel, TData, TKey> : IRepository<TModel, TKey>
        where TData : IEntity<TKey> where TModel : IEntity<TKey>
    {
        private readonly Dictionary<TKey, TData> items = new Dictionary<TKey, TData>();
        private readonly Dictionary<TKey, TData> itemsBeforeCommit = new Dictionary<TKey, TData>();
        private readonly IMapper mapper;
        private readonly object padLock = new object();

        public InMemoryRepository(IMapper mapper)
        {
            this.mapper = mapper;
        }

        public async Task<TModel> GetAsync(TKey key)
        {
            return await Task.Run(() => items.TryGetValue(key, out var existing) ? mapper.Map<TModel>(existing) : default);
        }

        public async Task<IList<TModel>> GetAllAsync()
        {
            return await Task.Run(() => (IList<TModel>) items.Values
                                                             .Select(x => mapper.Map<TModel>(x))
                                                             .ToList());
        }

        /// <summary>
        ///     This method should be called after using Create, Update or Delete to
        ///     commit the changes since the last call to this method.
        /// </summary>
        public async Task SaveChangesAsync()
        {
            await Task.Run(() =>
            {
                lock (padLock)
                {
                    foreach (var kvp in itemsBeforeCommit)
                    {
                        items[kvp.Key] = kvp.Value;
                    }

                    foreach (var kvp in items.Where(x => !itemsBeforeCommit.ContainsKey(x.Key)).ToList())
                    {
                        items.Remove(kvp.Key);
                    }

                    itemsBeforeCommit.Clear();
                }
            });
        }

        public async Task<TKey> CreateAsync(TModel model)
        {
            return await Task.Run(() =>
            {
                lock (padLock)
                {
                    itemsBeforeCommit[model.Id] = mapper.Map<TData>(model);
                    return model.Id;
                }
            });
        }

        public async Task UpdateAsync(TModel model)
        {
            await Task.Run(() =>
            {
                lock (padLock)
                {
                    itemsBeforeCommit[model.Id] = mapper.Map<TData>(model);
                }
            });
        }

        public async Task DeleteAsync(TModel model)
        {
            await Task.Run(() =>
            {
                lock (padLock)
                {
                    if (itemsBeforeCommit.ContainsKey(model.Id))
                    {
                        itemsBeforeCommit.Remove(model.Id);
                    }
                }
            });
        }

        public async Task<IList<TModel>> WhereAsync(Expression<Func<TModel, bool>> predicate)
        {
            var dataPredicate = mapper.Map<Expression<Func<TData, bool>>>(predicate)
                                      .Compile();

            return await Task.Run(() => (IList<TModel>) items.Values
                                                             .Where(dataPredicate)
                                                             .Select(x => mapper.Map<TModel>(x))
                                                             .ToList());
        }

        public void Dispose()
        {
            itemsBeforeCommit?.Clear();
        }
    }
}