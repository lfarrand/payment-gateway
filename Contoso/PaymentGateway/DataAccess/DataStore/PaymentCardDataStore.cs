using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;

namespace Contoso.PaymentGateway.DataAccess.DataStore
{
    public class PaymentCardDataStore : IDataStore<PaymentCard>
    {
        private readonly IRepositoryFactory repositoryFactory;

        public PaymentCardDataStore(IRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        public async Task<PaymentCard> GetAsync(string id)
        {
            using (var repo = repositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                return await repo.GetAsync(id);
            }
        }

        public async Task<IList<PaymentCard>> GetAllAsync()
        {
            using (var repo = repositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                return await repo.GetAllAsync();
            }
        }

        public async Task SaveChangesAsync()
        {
            using (var repo = repositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                await repo.SaveChangesAsync();
            }
        }

        public async Task<string> CreateAsync(PaymentCard paymentCard)
        {
            using (var repo = repositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                return await repo.CreateAsync(paymentCard);
            }
        }

        public async Task UpdateAsync(PaymentCard paymentCard)
        {
            using (var repo = repositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                await repo.UpdateAsync(paymentCard);
            }
        }

        public async Task DeleteAsync(PaymentCard paymentCard)
        {
            using (var repo = repositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                await repo.DeleteAsync(paymentCard);
            }
        }

        public async Task<IList<PaymentCard>> WhereAsync(Expression<Func<PaymentCard, bool>> predicate)
        {
            using (var repo = repositoryFactory.Create<PaymentCard, PaymentCardData, string>())
            {
                return await repo.WhereAsync(predicate);
            }
        }

        public void Dispose()
        {
            repositoryFactory?.Dispose();
        }
    }
}