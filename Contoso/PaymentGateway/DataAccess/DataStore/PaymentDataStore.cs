using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.Payments;

namespace Contoso.PaymentGateway.DataAccess.DataStore
{
    public class PaymentDataStore : IDataStore<Payment>
    {
        private readonly IRepositoryFactory repositoryFactory;

        public PaymentDataStore(IRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        public async Task<Payment> GetAsync(string id)
        {
            using (var repo = repositoryFactory.Create<Payment, PaymentData, string>())
            {
                return await repo.GetAsync(id);
            }
        }

        public async Task<IList<Payment>> GetAllAsync()
        {
            using (var repo = repositoryFactory.Create<Payment, PaymentData, string>())
            {
                return await repo.GetAllAsync();
            }
        }

        public async Task SaveChangesAsync()
        {
            using (var repo = repositoryFactory.Create<Payment, PaymentData, string>())
            {
                await repo.SaveChangesAsync();
            }
        }

        public async Task<string> CreateAsync(Payment payment)
        {
            using (var repo = repositoryFactory.Create<Payment, PaymentData, string>())
            {
                return await repo.CreateAsync(payment);
            }
        }

        public async Task UpdateAsync(Payment payment)
        {
            using (var repo = repositoryFactory.Create<Payment, PaymentData, string>())
            {
                await repo.UpdateAsync(payment);
            }
        }

        public async Task DeleteAsync(Payment payment)
        {
            using (var repo = repositoryFactory.Create<Payment, PaymentData, string>())
            {
                await repo.DeleteAsync(payment);
            }
        }

        public async Task<IList<Payment>> WhereAsync(Expression<Func<Payment, bool>> predicate)
        {
            using (var repo = repositoryFactory.Create<Payment, PaymentData, string>())
            {
                return await repo.WhereAsync(predicate);
            }
        }
    }
}