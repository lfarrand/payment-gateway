using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.CardPayments;

namespace Contoso.PaymentGateway.DataAccess.DataStore
{
    public class CardPaymentDataStore : IDataStore<CardPayment>
    {
        private readonly IRepositoryFactory repositoryFactory;

        public CardPaymentDataStore(IRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        public async Task<CardPayment> GetAsync(string id)
        {
            using (var repo = repositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                return await repo.GetAsync(id);
            }
        }

        public async Task<IList<CardPayment>> GetAllAsync()
        {
            using (var repo = repositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                return await repo.GetAllAsync();
            }
        }

        public async Task SaveChangesAsync()
        {
            using (var repo = repositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repo.SaveChangesAsync();
            }
        }

        public async Task<string> CreateAsync(CardPayment cardPayment)
        {
            using (var repo = repositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                return await repo.CreateAsync(cardPayment);
            }
        }

        public async Task UpdateAsync(CardPayment cardPayment)
        {
            using (var repo = repositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repo.UpdateAsync(cardPayment);
            }
        }

        public async Task DeleteAsync(CardPayment cardPayment)
        {
            using (var repo = repositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                await repo.DeleteAsync(cardPayment);
            }
        }

        public async Task<IList<CardPayment>> WhereAsync(Expression<Func<CardPayment, bool>> predicate)
        {
            using (var repo = repositoryFactory.Create<CardPayment, CardPaymentData, string>())
            {
                return await repo.WhereAsync(predicate);
            }
        }

        public void Dispose()
        {
            repositoryFactory?.Dispose();
        }
    }
}