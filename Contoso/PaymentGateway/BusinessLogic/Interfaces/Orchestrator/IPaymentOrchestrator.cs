using System.Threading.Tasks;
using Contoso.PaymentGateway.Models.Domain.Payments;

namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator
{
    public interface IPaymentOrchestrator
    {
        Task<GetPaymentResponse> GetPayment(GetPaymentRequest request);
    }
}