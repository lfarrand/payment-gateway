using System.Threading.Tasks;
using Contoso.PaymentGateway.Models.Domain.CardPayments;

namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator
{
    public interface ICardPaymentOrchestrator
    {
        Task<MakeCardPaymentResponse> MakeCardPayment(MakeCardPaymentRequest request);

        Task<GetCardPaymentResponse> GetCardPayment(GetCardPaymentRequest request);
    }
}