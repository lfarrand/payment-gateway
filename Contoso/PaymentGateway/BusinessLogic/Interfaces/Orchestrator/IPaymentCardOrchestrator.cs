﻿using System.Threading.Tasks;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;

namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator
{
    public interface IPaymentCardOrchestrator
    {
        Task<CreatePaymentCardResponse> CreatePaymentCard(CreatePaymentCardRequest request);

        Task<GetPaymentCardResponse> GetPaymentCard(GetPaymentCardRequest request);

        Task<GetAllPaymentCardsResponse> GetAllPaymentCards();
    }
}