namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments
{
    public interface IChargeCardResult
    {
        string UniqueId { get; }

        ChargeCardOperationStatus StatusCode { get; }
    }
}