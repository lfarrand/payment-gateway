using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;

namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments
{
    public interface ICardPaymentProcessor
    {
        ProcessPaymentResult ProcessPayment(PaymentCard paymentCard, Payment payment);
    }
}