using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;

namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments
{
    public interface ICardIssuerApi
    {
        IChargeCardResult ChargeCard(PaymentCard card, Payment payment);
    }
}