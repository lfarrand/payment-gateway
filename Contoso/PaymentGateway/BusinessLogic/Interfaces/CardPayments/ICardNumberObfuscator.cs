namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments
{
    public interface ICardNumberObfuscator
    {
        string ObfuscateCardNumber(string cardNumber);
    }
}