namespace Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments
{
    public enum ChargeCardOperationStatus
    {
        Success = 100,
        DeclinedNotEnoughFunds = 200,
        DeclinedCardExpired = 205,
        DeclinedCardHolderNameNotValid = 210,
        DeclinedCardAddressNotValid = 220,
        DeclinedCvvIncorrect = 230,
        DeclinedExpiryNotValid = 240,
        Unknown = 500
    }
}