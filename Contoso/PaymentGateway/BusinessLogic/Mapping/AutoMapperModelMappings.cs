using AutoMapper;
using Contoso.PaymentGateway.DataAccess.Model;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;

namespace Contoso.PaymentGateway.BusinessLogic.Mapping
{
    public class AutoMapperModelMappings : Profile
    {
        public AutoMapperModelMappings()
        {
            CreateMap<Payment, PaymentData>();
            CreateMap<PaymentData, Payment>();
            CreateMap<PaymentCard, PaymentCardData>()
               .ForMember(x => x.Payments,     opt => opt.Ignore())
               .ForMember(x => x.CardPayments, opt => opt.Ignore());
            CreateMap<PaymentCardData, PaymentCard>();
            CreateMap<CardPayment, CardPaymentData>();
            CreateMap<CardPaymentData, CardPayment>();
            DisableConstructorMapping();
        }
    }
}