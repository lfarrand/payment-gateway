using System;
using System.Collections.Generic;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Interfaces.Date;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;

namespace Contoso.PaymentGateway.BusinessLogic.CardPayments
{
    public class CardPaymentProcessor : ICardPaymentProcessor
    {
        private readonly ICardIssuerApi cardIssuerApi;
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly ILog log;
        private readonly Dictionary<ChargeCardOperationStatus, ProcessPaymentResultStatusCode> statusCodeMapping;

        public CardPaymentProcessor(ILog log, IDateTimeProvider dateTimeProvider,
                                    ICardIssuerApi cardIssuerApi,
                                    Dictionary<ChargeCardOperationStatus, ProcessPaymentResultStatusCode> statusCodeMapping)
        {
            this.log = log;
            this.dateTimeProvider = dateTimeProvider;
            this.cardIssuerApi = cardIssuerApi;
            this.statusCodeMapping = statusCodeMapping;
        }

        public ProcessPaymentResult ProcessPayment(PaymentCard paymentCard, Payment payment)
        {
            var result = new ProcessPaymentResult();

            try
            {
                var cardIssuerResponse = cardIssuerApi.ChargeCard(paymentCard, payment);

                result.PaymentId = payment.Id;
                result.UniqueId = cardIssuerResponse.UniqueId;
                result.Success = cardIssuerResponse.StatusCode == ChargeCardOperationStatus.Success;


                if (!statusCodeMapping.TryGetValue(cardIssuerResponse.StatusCode, out var statusCode))
                {
                    statusCode = ProcessPaymentResultStatusCode.Failed;
                }

                result.StatusCode = statusCode;

                if (cardIssuerResponse.StatusCode == ChargeCardOperationStatus.Success)
                {
                    result.Message =
                        $"Payment of {payment.Amount} {payment.Currency} for {payment.Description} was successfully processed at {dateTimeProvider.Current:dd-MMM-yyyy HH:mm:ss.fff}";
                }
                else
                {
                    result.Message =
                        $"Payment of {payment.Amount} {payment.Currency} for {payment.Description} was not processed";
                }
            }
            catch (Exception ex)
            {
                var msg =
                    $"An error occurred when processing payment {payment.Amount} {payment.Currency} for {payment.Description}";
                result.Message = msg;
                result.StatusCode = ProcessPaymentResultStatusCode.Failed;
                result.Success = false;
                log.Error(ex, msg);
            }

            return result;
        }
    }
}