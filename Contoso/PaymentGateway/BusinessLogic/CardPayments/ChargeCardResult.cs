using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;

namespace Contoso.PaymentGateway.BusinessLogic.CardPayments
{
    public class ChargeCardResult : IChargeCardResult
    {
        public ChargeCardResult(string uniqueId, ChargeCardOperationStatus statusCode)
        {
            UniqueId = uniqueId;
            StatusCode = statusCode;
        }

        public string UniqueId { get; set; }

        public ChargeCardOperationStatus StatusCode { get; set; }
    }
}