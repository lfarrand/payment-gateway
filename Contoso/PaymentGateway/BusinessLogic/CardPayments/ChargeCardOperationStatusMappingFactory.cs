using System.Collections.Generic;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Interfaces.Construction;

namespace Contoso.PaymentGateway.BusinessLogic.CardPayments
{
    public class
        ChargeCardOperationStatusMappingFactory : IFactory<
            Dictionary<ChargeCardOperationStatus, ProcessPaymentResultStatusCode>>
    {
        public Dictionary<ChargeCardOperationStatus, ProcessPaymentResultStatusCode> Create()
        {
            var statusCodeMapping = new Dictionary<ChargeCardOperationStatus, ProcessPaymentResultStatusCode>
            {
                [ChargeCardOperationStatus.Success] = ProcessPaymentResultStatusCode.Success,
                [ChargeCardOperationStatus.DeclinedCardExpired] = ProcessPaymentResultStatusCode.Failed,
                [ChargeCardOperationStatus.DeclinedCvvIncorrect] = ProcessPaymentResultStatusCode.Failed,
                [ChargeCardOperationStatus.DeclinedExpiryNotValid] = ProcessPaymentResultStatusCode.Failed,
                [ChargeCardOperationStatus.DeclinedNotEnoughFunds] = ProcessPaymentResultStatusCode.Failed,
                [ChargeCardOperationStatus.DeclinedCardAddressNotValid] = ProcessPaymentResultStatusCode.Failed,
                [ChargeCardOperationStatus.DeclinedCardHolderNameNotValid] = ProcessPaymentResultStatusCode.Failed
            };

            return statusCodeMapping;
        }
    }
}