using System.Linq;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;

namespace Contoso.PaymentGateway.BusinessLogic.CardPayments
{
    public class CardNumberObfuscator : ICardNumberObfuscator
    {
        public string ObfuscateCardNumber(string cardNumber)
        {
            if (string.IsNullOrWhiteSpace(cardNumber) || cardNumber.Length < 4)
            {
                return string.Concat(Enumerable.Repeat("X", 16));
            }

            return string.Concat(Enumerable.Repeat("X", cardNumber.Length - 4)) +
                   string.Concat(cardNumber.Skip(cardNumber.Length - 4).Take(4));
        }
    }
}