using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Interfaces.Identity;

namespace Contoso.PaymentGateway.BusinessLogic.CardPayments
{
    public class DummyCardIssuerApi : ICardIssuerApi
    {
        private readonly IIdentityGenerator<string> idGenerator;

        public DummyCardIssuerApi(IIdentityGenerator<string> idGenerator)
        {
            this.idGenerator = idGenerator;
        }

        public IChargeCardResult ChargeCard(PaymentCard card, Payment payment)
        {
            return new ChargeCardResult(idGenerator.GenerateId(), ChargeCardOperationStatus.Success);
        }
    }
}