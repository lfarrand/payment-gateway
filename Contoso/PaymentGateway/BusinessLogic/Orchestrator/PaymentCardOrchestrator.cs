using System;
using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Utilities.Interfaces.Date;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;

namespace Contoso.PaymentGateway.BusinessLogic.Orchestrator
{
    public class PaymentCardOrchestrator : IPaymentCardOrchestrator
    {
        private readonly IDateTimeProvider dateTimeProvider;
        private readonly ILog log;
        private readonly IDataStore<PaymentCard> paymentCardDatastore;

        public PaymentCardOrchestrator(ILog log, IDateTimeProvider dateTimeProvider,
                                       IDataStore<PaymentCard> paymentCardDatastore)
        {
            this.log = log;
            this.dateTimeProvider = dateTimeProvider;
            this.paymentCardDatastore = paymentCardDatastore;
        }

        public async Task<CreatePaymentCardResponse> CreatePaymentCard(CreatePaymentCardRequest request)
        {
            var response = new CreatePaymentCardResponse();

            try
            {
                response.PaymentCardId = await paymentCardDatastore.CreateAsync(request.PaymentCard);
                await paymentCardDatastore.SaveChangesAsync();

                response.Success = true;

                log.Debug(
                          $"Created payment card {response.PaymentCardId} at {dateTimeProvider.Current:dd-MMM-yyyy HH:mm:ss.fff}");
            }
            catch (Exception ex)
            {
                log.Error(ex, "Failed to create payment card");
                response.Success = false;
            }

            return response;
        }

        public async Task<GetPaymentCardResponse> GetPaymentCard(GetPaymentCardRequest request)
        {
            var response = new GetPaymentCardResponse();

            try
            {
                response.PaymentCard = await paymentCardDatastore.GetAsync(request.PaymentCardId);
                response.Success = true;
            }
            catch (Exception ex)
            {
                log.Error(ex, "Failed to get payment card");
                response.Success = false;
            }

            return response;
        }

        public async Task<GetAllPaymentCardsResponse> GetAllPaymentCards()
        {
            var response = new GetAllPaymentCardsResponse();

            try
            {
                response.PaymentCards = await paymentCardDatastore.GetAllAsync();
                response.Success = true;
            }
            catch (Exception ex)
            {
                log.Error(ex, "Failed to get all payment cards");
                response.Success = false;
            }

            return response;
        }
    }
}