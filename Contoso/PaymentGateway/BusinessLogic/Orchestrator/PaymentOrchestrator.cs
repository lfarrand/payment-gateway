using System;
using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;

namespace Contoso.PaymentGateway.BusinessLogic.Orchestrator
{
    public class PaymentOrchestrator : IPaymentOrchestrator
    {
        private readonly ILog log;
        private readonly IDataStore<Payment> paymentDatastore;

        public PaymentOrchestrator(ILog log, IDataStore<Payment> paymentDatastore)
        {
            this.log = log;
            this.paymentDatastore = paymentDatastore;
        }

        public async Task<GetPaymentResponse> GetPayment(GetPaymentRequest request)
        {
            var response = new GetPaymentResponse();

            try
            {
                response.Payment = await paymentDatastore.GetAsync(request.PaymentId);
                response.Success = true;
            }
            catch (Exception ex)
            {
                log.Error(ex, "Failed to get payment");
                response.Success = false;
            }

            return response;
        }
    }
}