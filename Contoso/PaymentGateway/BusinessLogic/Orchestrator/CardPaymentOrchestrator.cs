using System;
using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.DataAccess.Interfaces.Concurrency;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;

namespace Contoso.PaymentGateway.BusinessLogic.Orchestrator
{
    public class CardPaymentOrchestrator : ICardPaymentOrchestrator
    {
        private readonly ICardNumberObfuscator cardNumberObfuscator;
        private readonly IDataStore<CardPayment> cardPaymentDatastore;
        private readonly ICardPaymentProcessor cardPaymentProcessor;
        private readonly ILockManager lockManager;
        private readonly ILog log;
        private readonly IDataStore<PaymentCard> paymentCardDatastore;
        private readonly IDataStore<Payment> paymentDatastore;

        public CardPaymentOrchestrator(ILog log, ICardPaymentProcessor cardPaymentProcessor,
                                       IDataStore<CardPayment> cardPaymentDatastore, IDataStore<PaymentCard> paymentCardDatastore,
                                       IDataStore<Payment> paymentDatastore, ICardNumberObfuscator cardNumberObfuscator, ILockManager lockManager)
        {
            this.log = log;
            this.cardPaymentProcessor = cardPaymentProcessor;
            this.cardPaymentDatastore = cardPaymentDatastore;
            this.paymentCardDatastore = paymentCardDatastore;
            this.paymentDatastore = paymentDatastore;
            this.cardNumberObfuscator = cardNumberObfuscator;
            this.lockManager = lockManager;
        }

        public async Task<MakeCardPaymentResponse> MakeCardPayment(MakeCardPaymentRequest request)
        {
            var response = new MakeCardPaymentResponse();

            try
            {
                if (request.Payment == null)
                {
                    throw new ArgumentNullException("request.Payment",
                                                    "Payment was not provided and is a mandatory parameter");
                }

                if (string.IsNullOrWhiteSpace(request.PaymentCardId))
                {
                    throw new ArgumentNullException("request.PaymentCardId",
                                                    "Payment card id was not provided and is a mandatory parameter");
                }

                // Lock the payment card to ensure it doesn't get updated by something else during payment processing
                using (lockManager.GetLockAsync($"PaymentCard:{request.PaymentCardId}"))
                {
                    var paymentCard = await paymentCardDatastore.GetAsync(request.PaymentCardId);

                    if (paymentCard != null)
                    {
                        var paymentId = await paymentDatastore.CreateAsync(request.Payment);

                        var result = cardPaymentProcessor.ProcessPayment(paymentCard, request.Payment);

                        response.PaymentId = paymentId;
                        response.Message = result.Message;
                        response.UniqueId = result.UniqueId;

                        if (result.Success)
                        {
                            response.ResponseCode = MakePaymentResponseCode.Success;
                            response.Success = true;
                        }
                        else
                        {
                            response.ResponseCode = MakePaymentResponseCode.FailedPaymentCardIssuerError;
                            response.Success = false;
                        }
                    }
                    else
                    {
                        response.ResponseCode = MakePaymentResponseCode.FailedPaymentCardNotFound;
                        response.Success = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex, "Failed to make payment");
                response.Message = "An unknown error occurred";
                response.ResponseCode = MakePaymentResponseCode.FailedUnknownError;
                response.Success = false;
            }

            return response;
        }

        public async Task<GetCardPaymentResponse> GetCardPayment(GetCardPaymentRequest request)
        {
            var response = new GetCardPaymentResponse();

            try
            {
                var cardPayment = await cardPaymentDatastore.GetAsync(request.CardPaymentId);

                // Obfuscate the card number to prevent leaking sensitive card info
                cardPayment.PaymentCard.CardNumber =
                    cardNumberObfuscator.ObfuscateCardNumber(cardPayment.PaymentCard.CardNumber);

                response.CardPayment = cardPayment;
                response.Success = true;
            }
            catch (Exception ex)
            {
                log.Error(ex, "Failed to get card payment");
                response.Success = false;
            }

            return response;
        }
    }
}