﻿using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Contoso.PaymentGateway.BusinessLogic.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.CardPayments;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.BusinessLogic.Mapping;
using Contoso.PaymentGateway.BusinessLogic.Orchestrator;
using Contoso.PaymentGateway.DataAccess.DataStore;
using Contoso.PaymentGateway.DataAccess.EntityFramework.DatabaseContext;
using Contoso.PaymentGateway.DataAccess.EntityFramework.Repository;
using Contoso.PaymentGateway.DataAccess.Interfaces.DataStore;
using Contoso.PaymentGateway.DataAccess.Interfaces.Repository;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Date;
using Contoso.PaymentGateway.Utilities.Identity;
using Contoso.PaymentGateway.Utilities.Interfaces.Date;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Contoso.PaymentGateway.WebApi
{
    public class Startup
    {
        private readonly ILog log;

        public Startup(ILog log, IConfiguration configuration)
        {
            this.log = log;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                    .AddMetrics();

            // Auto Mapper Configurations
            log.Debug("Creating AutoMapper mappings");

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperModelMappings());
                mc.AddExpressionMapping();
            });

            var dbContextFactory = new SqlLitePaymentGatewayDbContextFactory();

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddSingleton(dbContextFactory);
            services.AddSingleton<IRepositoryFactory, EntityFrameworkRepositoryFactory>(s =>
                new EntityFrameworkRepositoryFactory(s.GetService<IMapper>(), dbContextFactory,
                                                     new EntityFrameworkRepositoriesRegistry().Create()));
            services.AddSingleton<IDataStore<PaymentCard>, PaymentCardDataStore>();
            services.AddSingleton<IDataStore<Payment>, PaymentDataStore>();
            services.AddSingleton<IDataStore<CardPayment>, CardPaymentDataStore>();
            services.AddSingleton<ICardPaymentOrchestrator, CardPaymentOrchestrator>();
            services.AddSingleton<IPaymentOrchestrator, PaymentOrchestrator>();
            services.AddSingleton<IPaymentCardOrchestrator, PaymentCardOrchestrator>();
            services.AddSingleton<IDateTimeProvider, DateTimeProvider>();
            services.AddSingleton<ICardIssuerApi, DummyCardIssuerApi>(s => new DummyCardIssuerApi(new StringGuidIdentityGenerator(new GuidIdentityGenerator())));
            services.AddSingleton<ICardNumberObfuscator, CardNumberObfuscator>();
            services.AddSingleton(new ChargeCardOperationStatusMappingFactory().Create());
            services.AddSingleton<ICardPaymentProcessor, CardPaymentProcessor>();

            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "Payment Gateway API", Version = "v1"}); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            log.Debug($"Configuring for {env.EnvironmentName} environment");

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Payment Gateway API V1");
                c.RoutePrefix = string.Empty;
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}