﻿using App.Metrics.AspNetCore;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;
using Contoso.PaymentGateway.Utilities.Logging;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Contoso.PaymentGateway.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                          .ConfigureServices(s =>
                           {
                               s.AddSingleton<ILog, SeriLogLogger>(sp =>
                                                                       new SeriLogLogger(sp.GetRequiredService<IConfiguration>()));
                           })
                          .UseMetrics()
                          .UseStartup<Startup>();
        }
    }
}