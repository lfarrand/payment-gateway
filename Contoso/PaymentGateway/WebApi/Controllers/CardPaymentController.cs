using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.Models.Domain.CardPayments;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Contoso.PaymentGateway.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardPaymentController : ControllerBase
    {
        private readonly ICardPaymentOrchestrator cardPaymentOrchestrator;
        private readonly ILog log;

        public CardPaymentController(ILog log, ICardPaymentOrchestrator cardPaymentOrchestrator)
        {
            this.log = log;
            this.cardPaymentOrchestrator = cardPaymentOrchestrator;
        }

        [HttpGet("{id}")]
        public async Task<GetCardPaymentResponse> GetCardPayment(string id)
        {
            log.Debug($"CardPaymentController.GetCardPayment called with id = {id}");
            var request = new GetCardPaymentRequest {CardPaymentId = id};
            var response = await cardPaymentOrchestrator.GetCardPayment(request);
            return response;
        }

        [HttpPost]
        public async Task<MakeCardPaymentResponse> MakeCardPayment(string paymentCardId, [FromBody] Payment payment)
        {
            log.Debug($"CardPaymentController.MakeCardPayment called with paymentCardId = {paymentCardId}");
            var request = new MakeCardPaymentRequest {Payment = payment, PaymentCardId = paymentCardId};
            var response = await cardPaymentOrchestrator.MakeCardPayment(request);
            return response;
        }
    }
}