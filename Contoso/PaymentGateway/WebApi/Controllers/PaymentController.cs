using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.Models.Domain.Payments;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Contoso.PaymentGateway.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly ILog log;
        private readonly IPaymentOrchestrator paymentOrchestrator;

        public PaymentController(ILog log, IPaymentOrchestrator paymentOrchestrator)
        {
            this.log = log;
            this.paymentOrchestrator = paymentOrchestrator;
        }

        [HttpGet("{id}")]
        public async Task<GetPaymentResponse> GetPayment(string id)
        {
            log.Debug($"PaymentController.GetPaymentCard called with id = {id}");
            var request = new GetPaymentRequest {PaymentId = id};
            var response = await paymentOrchestrator.GetPayment(request);
            return response;
        }
    }
}