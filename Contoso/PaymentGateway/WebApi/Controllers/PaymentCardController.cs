using System.Threading.Tasks;
using Contoso.PaymentGateway.BusinessLogic.Interfaces.Orchestrator;
using Contoso.PaymentGateway.Models.Domain.PaymentCards;
using Contoso.PaymentGateway.Utilities.Interfaces.Logging;
using Microsoft.AspNetCore.Mvc;

namespace Contoso.PaymentGateway.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentCardController : ControllerBase
    {
        private readonly ILog log;
        private readonly IPaymentCardOrchestrator paymentCardOrchestrator;

        public PaymentCardController(ILog log, IPaymentCardOrchestrator paymentCardOrchestrator)
        {
            this.log = log;
            this.paymentCardOrchestrator = paymentCardOrchestrator;
        }

        [HttpGet("{id}")]
        public async Task<GetPaymentCardResponse> GetPaymentCard(string id)
        {
            log.Debug($"PaymentCardController.GetPaymentCard called with id = {id}");
            var request = new GetPaymentCardRequest {PaymentCardId = id};
            var response = await paymentCardOrchestrator.GetPaymentCard(request);
            return response;
        }

        [HttpGet]
        public async Task<GetAllPaymentCardsResponse> GetPaymentCard()
        {
            log.Debug("PaymentCardController.GetPaymentCard called");
            var response = await paymentCardOrchestrator.GetAllPaymentCards();
            return response;
        }

        [HttpPost]
        public async Task<CreatePaymentCardResponse> CreatePaymentCard([FromBody] PaymentCard paymentCard)
        {
            log.Debug($"PaymentCardController.CreatePaymentCard called with paymentCard.Id = {paymentCard.Id}");
            var request = new CreatePaymentCardRequest {PaymentCard = paymentCard};
            var response = await paymentCardOrchestrator.CreatePaymentCard(request);
            return response;
        }
    }
}