FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS base
WORKDIR /app
EXPOSE 5000
ENV ASPNETCORE_URLS=http://+:5000
#;https://+:5001

FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /src
COPY Contoso.PaymentGateway.sln ./
COPY Contoso/PaymentGateway/BusinessLogic/*.csproj ./Contoso/PaymentGateway/BusinessLogic/
COPY Contoso/PaymentGateway/BusinessLogic/Interfaces/*.csproj ./Contoso/PaymentGateway/BusinessLogic/Interfaces/
COPY Contoso/PaymentGateway/DataAccess/*.csproj ./Contoso/PaymentGateway/DataAccess/
COPY Contoso/PaymentGateway/DataAccess/EntityFramework/*.csproj ./Contoso/PaymentGateway/DataAccess/EntityFramework/
COPY Contoso/PaymentGateway/DataAccess/EntityFramework/Interfaces/*.csproj ./Contoso/PaymentGateway/DataAccess/EntityFramework/Interfaces/
COPY Contoso/PaymentGateway/DataAccess/Interfaces/*.csproj ./Contoso/PaymentGateway/DataAccess/Interfaces/
COPY Contoso/PaymentGateway/Models/*.csproj ./Contoso/PaymentGateway/Models/
COPY Contoso/PaymentGateway/Models/Interfaces/*.csproj ./Contoso/PaymentGateway/Models/Interfaces/
COPY Contoso/PaymentGateway/TestClient/*.csproj ./Contoso/PaymentGateway/TestClient/
COPY Contoso/PaymentGateway/Tests/Integration/*.csproj ./Contoso/PaymentGateway/Tests/Integration/
COPY Contoso/PaymentGateway/Tests/Unit/*.csproj ./Contoso/PaymentGateway/Tests/Unit/
COPY Contoso/PaymentGateway/Utilities/*.csproj ./Contoso/PaymentGateway/Utilities/
COPY Contoso/PaymentGateway/Utilities/Interfaces/*.csproj ./Contoso/PaymentGateway/Utilities/Interfaces/
COPY Contoso/PaymentGateway/WebApi/*.csproj ./Contoso/PaymentGateway/WebApi/

RUN ["dotnet", "restore"]
COPY . .
WORKDIR /src/Contoso/PaymentGateway/WebApi
RUN dotnet build -c Release -o /app

FROM build AS publish
RUN dotnet publish -c Release -o /app

# Build runtime image
FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Contoso.PaymentGateway.WebApi.dll", "--environment=Development"]